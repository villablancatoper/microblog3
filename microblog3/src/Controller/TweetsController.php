<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\View\Helper\UrlHelper;
use App\Controller\Component\UsersComponent;
use Cake\Collection\Collection;
use Cake\Controller\ComponentRegistry;

/**
 * Tweets Controller
 *
 * @property \App\Model\Table\TweetsTable $Tweets
 *
 * @method \App\Model\Entity\Tweet[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TweetsController extends AppController
{
    /**
     * Initialization
     * 
     * 
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Followers');
        $this->loadModel('Likes');
        $this->loadModel('Tweets');
        $this->loadModel('Comments');

        $registry = new ComponentRegistry();
        $this->userComp = new UsersComponent($registry);
    }


    /**
     * delete post
     * this deletes the post of the current user
     * Integer $id tweet id
     *
     */
    public function delete($id) {
        $this->autoRender = false;

        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        $delete = $this->Tweets->find()
            ->where([
                'Tweets.id' => $id
            ])
            ->first();

        if (!empty($delete)){
            $table = TableRegistry::get('Tweets');
            $tweetsTable = $table->get($id);

            if($this->Tweets->delete($tweetsTable)){
                $this->Flash->success(__('Tweet successfully deleted!'));
                return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
            }
        }
    }

    public function save() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $tweets = $this->Tweets->newEntity();
            $comments = $this->Comments->newEntity();
            $data = $this->request->getData();
            $dataNew[] = $this->request->getData();


            if (!empty($data['edit_indicator'])) {
                $tweet = $this->Tweets->get($data['edit_indicator']);
                unset($data['edit_indicator']);

                $data = $this->Tweets->patchEntity($tweet, $data);

                if ($this->Tweets->save($data)) {
                    $this->Flash->success(__('Your post has been updated.'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
                }
                $this->Flash->error(__('Unable to update your post.'));
            }

            if (empty($data['picture']['name'])) {
                if(empty($data['content'])) {
                    $this->Flash->error(__('Please enter something before continuing'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
                }
            }

            if (isset($data['picture']) || isset($data['content'])) {
                $filename = '';

                $picture = $this->userComp->uploadPhoto(
                    collection($dataNew)
                );

                if ($picture === 'extension') {
                    $this->Flash->error(__('Invalid Extension'));
                    return $this->redirect($this->referer());
                }
    
    
                if ($picture === 'path') {
                    $this->Flash->error(__('Something went wrong with the path, please try again'));
                    return $this->redirect($this->referer());
                }
    
    
                if ($picture === 'size') {
                    $this->Flash->error(__('Image size too large, upload only 8MB or less'));
                    return $this->redirect($this->referer());
                }

                $tweets->user_id = $data['user_id'];
                $tweets->picture = $picture;
                $tweets->content = $data['content'];

                if ($this->Tweets->save($tweets)) {
                    return $this->redirect($this->redirect('/users/dashboard'));
                }
            }

        }
    }

    public function retweet() {
        if ($this->request->is('post')) {
            $this->autoRender = false;
            $tweets = $this->Tweets->newEntity();
            $data = $this->request->getData();

            $tweets->retweet_content = $data['retweet_content'];
            $tweets->retweet_user_id = $data['retweet_user_id'];
            $tweets->content = $data['content'];
            $tweets->retweet_id = $data['retweet_id'];
            $tweets->retweet_picture = $data['retweet_picture'];
            $tweets->user_id = $data['user_id'];

            if ($this->Tweets->save($tweets)) {
                return $this->redirect($this->referer());
            }
        }
    }
}

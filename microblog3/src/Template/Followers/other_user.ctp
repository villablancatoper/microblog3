<div class="container emp-profile">
<?php echo $this->Flash->render()?>
    <form method="post">
        <div class="row">
            <div class="col-md-4">
                <div class="profile-img">
                    <?= $this->Html->image("/img/users/" . h($Other['User']['image']));?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="profile-head">
                    <h5>
                        <?php echo h($Other['User']['name']); ?>
                    </h5>
                    <h6>
                        <?php echo h($Other['User']['location']); ?>
                    </h6>
                    <h5 style="display: none;"><?php echo $id = h($Other['User']['id']); ?></h5>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Timeline</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">About</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
            <?php if (!empty($findFollowed)) { ?>
                <?php if ($findFollowed['Follower']['status'] == 1 ) { ?>
                    <button type="button" class="btn" id="follow" name="follow">Followed
                <?php } else { ?>
                    <button type="button" class="btn btn-primary" id="follow" name="follow">Follow
                <?php } ?>
            <?php } else { ?>
                <button type="button" class="btn btn-primary" id="follow" name="follow">Follow
            <?php }?>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="h6 text-muted">Followers</div>
                        <div class="h5"><?php echo $isFollowing; ?></div>
                    </li>
                    <li class="list-group-item">
                        <div class="h6 text-muted">Following</div>
                        <div class="h5"><?php echo $isFollowed; ?></div>
                    </li>
                    <li class="list-group-item">
                        <a href="<?php echo $url ?>users/dashboard">
                            <i class="fa fa-home"></i>
                            Home
                        </a>
                    </li>
                    <li class="list-group-item active" style="background-color: #80e4e3db">
                        <a href="<?php echo $url ?>users/profile">
                            <i class="fa fa-user"></i>
                            Profile 
                        </a>
                    </li>
                    <li class="list-group-item">
                        <!-- <a href="<?php echo $url ?>users/other_user_follower">
                            
                            Followers 
                        </a> -->
                        <i class="fa fa-users"></i>
                        <?php 
                            echo $this->Html->Link('Followers', array(
                                'label' => false,
                                'action' => 'other_user_follower', $id
                                )
                            )
                        ?>
                    </li>
                </ul>
            </div>
            <div class="col-md-7">
                <div class="tab-content profile-tab" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <?php foreach ($tweet as $t): ?>
                            <?php ?>
                                <br />                 
                                    <!-- if card is retweeted -->
                                    <?php if (!empty($t['Tweet']['retweet_id'])) { ?>
                                        <div class="card gedf-card"> 
                                            <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div class="mr-2">
                                                            <?= $this->Html->image("/img/users/" . h($t['Retweeter']['image']), ['class' => 'rounded-circle', 'width' => '45']);?>
                                                        </div>
                                                        <div class="ml-2">
                                                            <div class="h5 m-0"><?php echo h($t['Retweeter']['username']); ?> retweeted: </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    <?php if ($t['User']['username'] == $User['User']['username']) { ?>
                                                        <div class="dropdown">
                                                            <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-ellipsis-h"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                                            <?php echo $this->Form->postLink(
                                                                'Delete',
                                                                array('class' => 'dropdown-item', 'action' => 'delete', $t['Tweet']['id']),
                                                                array('confirm' => 'Are you sure?')
                                                                );
                                                            ?>
                                                            </div>
                                                        </div>
                                                    <?php } else { ?>
                                                    <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div>
                                                    <p class="card-text">
                                                        <?php echo h($t['Tweet']['retweet_content']); ?>
                                                    </p>
                                                </div>
                                                <hr>
                                                    <div class="card gedf-card offset-1">   
                                                        <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <div class="d-flex justify-content-between align-items-center">
                                                                    <div class="mr-2">
                                                                        <?= $this->Html->image("/img/users/" . $t['User']['image'], ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                    </div>
                                                                    <div class="ml-2">
                                                                        <div class="h5 m-0"><?php echo h($t['User']['username']); ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-body">
                                                            <div>
                                                            <?= $this->Html->image("/img/users/" . $t['Tweet']['retweet_picture'], ['class' => 'box', 'width' => '300', 'height' => '300']);?>
                                                            </div>
                                                            <p class="card-text">
                                                                <?php echo h($t['Tweet']['content']); ?>
                                                            </p>
                                                    </div>
                                            </div>
                                            <div class="card-footer">
                                                <!-- Tweet Utilites -->
                                                        <p> Created: <?php echo $this->Time->niceshort($t['Tweet']['created']); ?> </p>
                                                        <?php $ctr = 0 ?>
                                                        <?php foreach ($isLiked as $il):?>
                                                            <?php if ($il['Like']['tweet_id'] == $t['Tweet']['id']) {
                                                                
                                                            }
                                                            ?>
                                                        <?php endforeach ?>
                                                        <?php foreach ($count as $c):?>
                                                            <?php if ($c['Like']['tweet_id'] == $t['Tweet']['id']) {
                                                                $ctr++;
                                                            }
                                                            ?>
                                                        <?php endforeach ?>
                                                        <span class="fa fa-thumbs-up" id="likeCount"><?php echo $ctr; ?></span> 
                                                        <br />
                                                        <hr>
                                                        <?php if (!empty($t['Comment'])) :?>
                                                            <a href="#a" id="more<?php echo $t['Tweet']['id']?>">View Replies</a>
                                                        <?php endif; ?>
                                                        <div id="replies<?php echo $t['Tweet']['id']?>" class="form-group-replies" style="display: none">
                                                        <!-- Comment Section -->
                                                        <?php foreach ($commentTweet as $c): ?>
                                                            <div id="#a reply-header<?php echo $t['Tweet']['id']?>">
                                                                <div class="card gedf-card">
                                                                    <?php if ($t['Tweet']['id'] == $c['Tweet']['id']) { ?>
                                                                        <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                                            <div class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex justify-content-between align-items-center">
                                                                                    <div class="mr-2">
                                                                                    <?= $this->Html->image("/img/users/" . $t['User']['image'], ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                                    </div>
                                                                                    <div class="ml-2">
                                                                                        <div class="h5 m-0"><?php echo h($c['User']['username']); ?></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                                    
                                                                        <div class="card-body">
                                                                            <p class="card-text">
                                                                                <?php echo h($c['Comment']['comment']); ?>
                                                                            </p>
                                                                        </div>
                                                                        <hr>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php endforeach ?>
                                                        <!-- COmment Section end -->
                                                        </div>
                                                        <br />
                                                        <br />
                                                            <?php if (isset($il['Like']['tweet_id'] ) && $il['Like']['tweet_id'] == $t['Tweet']['id']) :?>
                                                                <button id="like_post<?php echo $t['Tweet']['id']?>" class="btn btn-outline-secondary">
                                                                    <span class="fa fa-thumbs-o-up fa-thumbs-up"></span> Liked
                                                                </button>
                                                            <?php else :?>
                                                                <button id="like_post<?php echo $t['Tweet']['id']?>" class="btn btn-outline-secondary likeChange">
                                                                    <span class="fa fa-thumbs-o-up"></span> Like
                                                                </button>
                                                            <?php endif; ?>
                                                        <button id="formButton<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                        <span class="fa fa-comment"></span> Comment
                                                        </button>
                                                        <button id="formRetweet<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                        <span class="fa fa-retweet"></span> Retweet
                                                        </button>
                                                        <?php if ($t['User']['username'] == $User['User']['username']) { ?>
                                                            <button id="formButton<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                            <span class="fa fa-edit"></span>
                                                            <?php echo $this->Html->Link('Edit', array(
                                                                'label' => false,
                                                                'class' => 'btn btn-outline-secondary',
                                                                'role' => 'button',
                                                                'action' => 'edit', $t['Tweet']['id']
                                                                    )
                                                                );
                                                            ?>
                                                        <?php } else { ?>
                                                        <?php } ?>
                                                        </button>
                                                        <div id="reply-body<?php echo $t['Tweet']['id']?>" class="form-group-reply" style="display: none">
                                                                <label class="sr-only" for="message">post</label>
                                                                <?php echo $this->Form->create('Comment', [
                                                                    'id' => 'reply'. $t['Tweet']['id']
                                                                    ]);
                                                                    echo $this->Form->input('comment', array(
                                                                    'label' => false,
                                                                    'type' => 'text',
                                                                    'text' => '',
                                                                    'rows' => '3',
                                                                    'maxlength' => '140',
                                                                    'class' => 'form-control',
                                                                    'placeholder' => 'Please enter up to 140 characters',
                                                                    'style' => 'margin-top: 10px'));
                                                                    echo $this->Form->input('user_id',
                                                                    ['id' => 'user_id',
                                                                    'value' => $User['User']['id'],
                                                                    'type' => 'hidden',
                                                                    'label' => false]);
                                                                    echo $this->Form->input('tweet_id',
                                                                    ['id' => 'tweet_id',
                                                                    'value' => $t['Tweet']['id'],
                                                                    'type' => 'hidden',
                                                                    'label' => false]);
                                                                ?>
                                                                <div id="inputButton">
                                                                    <?= $this->Form->input('Reply',
                                                                    ['type' => 'button',
                                                                    'label' => false,
                                                                    'id' => 'submitReply',
                                                                    'class'=>'btn btn-primary float-right'
                                                                    ]);?>
                                                                <?= $this->Form->end() ?>
                                                            </div>
                                                        </div>
                                                        
                                                <!-- Tweet Utilities End -->
                                            </div>                                
                                            </div>
                                        </div>
                                    <!-- if tweet is original -->                                            
                                    <?php } else { ?>
                                        <div class="card gedf-card">   
                                            <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div class="mr-2">
                                                        <?= $this->Html->image("/img/users/" . $t['User']['image'], ['class' => 'rounded-circle', 'width' => '45']);?>
                                                        </div>
                                                        <div class="ml-2">
                                                            <div class="h5 m-0"><?php echo $t['User']['username']; ?></div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <?php if ($t['User']['username'] == $User['User']['username']) { ?>
                                                            <div class="dropdown">
                                                                <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="fa fa-ellipsis-h"></i>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                                                    <?php echo $this->Form->postLink(
                                                                        'Delete',
                                                                        array('class' => 'dropdown-item', 'action' => 'delete', $t['Tweet']['id']),
                                                                        array('confirm' => 'Are you sure?')
                                                                        );
                                                                ?>
                                                                </div>
                                                            </div>
                                                        <?php } else { ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div>
                                                <?= $this->Html->image("/img/users/" . $t['Tweet']['picture'], ['class' => '', 'width' => '300', 'height' => '300']);?>
                                                </div>
                                                <p class="card-text">
                                                    <?php echo h($t['Tweet']['content']); ?>
                                                </p>
                                            </div>                                        
                                            <div class="card-footer">
                                                <!-- Tweet Utilites -->
                                                        <p> Created: <?php echo $this->Time->niceshort($t['Tweet']['created']); ?> </p>
                                                        <?php $ctr = 0 ?>
                                                        <?php foreach ($isLiked as $il):?>
                                                            <?php if ($il['Like']['tweet_id'] == $t['Tweet']['id']) {
                                                                $ctr++;
                                                            }
                                                            ?>
                                                        <?php endforeach ?>
                                                        <span class="fa fa-thumbs-up" id="likeCount"><?php echo $ctr?></span>
                                                        <br />
                                                        <hr>
                                                        <?php if (!empty($t['Comment'])) :?>
                                                            <a href="#a" id="more<?php echo $t['Tweet']['id']?>">View Replies</a>
                                                        <?php endif; ?>
                                                        <div id="replies<?php echo $t['Tweet']['id']?>" class="form-group-replies" style="display: none">
                                                        <?php foreach ($commentTweet as $c): ?>
                                                            <div id="#a reply-header<?php echo $t['Tweet']['id']?>">
                                                                <div class="card gedf-card">
                                                                    <?php if ($t['Tweet']['id'] == $c['Tweet']['id']) { ?>
                                                                        <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                                            <div class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex justify-content-between align-items-center">
                                                                                    <div class="mr-2">
                                                                                    <?= $this->Html->image("/img/users/" . $t['User']['image'], ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                                    </div>
                                                                                    <div class="ml-2">
                                                                                        <div class="h5 m-0"><?php echo h($c['User']['username']); ?></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                                    
                                                                        <div class="card-body">
                                                                            <p class="card-text">
                                                                                <?php echo h($c['Comment']['comment']); ?>
                                                                            </p>
                                                                        </div>
                                                                        <hr>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php endforeach ?>
                                                        </div>
                                                        <br />
                                                        <br />
                                                        <?php if (isset($il['Like']['tweet_id'] ) && $il['Like']['tweet_id'] == $t['Tweet']['id']) :?>
                                                            <button id="like_post<?php echo $t['Tweet']['id']?>" class="btn btn-outline-secondary">
                                                                <span class="fa fa-thumbs-o-up fa-thumbs-up"></span> Liked
                                                            </button>
                                                        <?php else :?>
                                                            <button id="like_post<?php echo $t['Tweet']['id']?>" class="btn btn-outline-secondary likeChange">
                                                                <span class="fa fa-thumbs-o-up"></span> Like
                                                            </button>
                                                        <?php endif; ?>
                                                        <button id="formButton<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                            <span class="fa fa-comment"></span> Comment 
                                                        </button>
                                                        <button id="formRetweet<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                            <span class="fa fa-retweet"></span> Retweet
                                                        </button>
                                                        <?php if ($t['User']['username'] == $User['User']['username']) { ?>
                                                            <button id="formButton<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                            <span class="fa fa-edit"></span>
                                                            <?php echo $this->Html->Link('Edit', array(
                                                                'label' => false,
                                                                'class' => 'btn btn-outline-secondary',
                                                                'role' => 'button',
                                                                'action' => 'edit', $t['Tweet']['id']
                                                                    )
                                                                );
                                                            ?>
                                                        <?php } else { ?>
                                                        <?php } ?>
                                                        </button>
                                                        <div id="reply-body<?php echo $t['Tweet']['id']?>" class="form-group-reply" style="display: none">
                                                                <label class="sr-only" for="message">post</label>
                                                                <?php echo $this->Form->create('Comment', [
                                                                    'id' => 'reply'. $t['Tweet']['id']
                                                                    ]);
                                                                    echo $this->Form->input('comment', array(
                                                                    'label' => false,
                                                                    'type' => 'text',
                                                                    'text' => '',
                                                                    'rows' => '3',
                                                                    'maxlength' => '140',
                                                                    'class' => 'form-control',
                                                                    'placeholder' => 'Please enter up to 140 characters',
                                                                    'style' => 'margin-top: 10px'));
                                                                    echo $this->Form->input('user_id',
                                                                    ['id' => 'user_id',
                                                                    'value' => $User['User']['id'],
                                                                    'type' => 'hidden',
                                                                    'label' => false]);
                                                                    echo $this->Form->input('tweet_id',
                                                                    ['id' => 'tweet_id',
                                                                    'value' => $t['Tweet']['id'],
                                                                    'type' => 'hidden',
                                                                    'label' => false]);
                                                                ?>
                                                                <div id="inputButton">
                                                                    <?= $this->Form->input('Reply',
                                                                    ['type' => 'button',
                                                                    'label' => false,
                                                                    'id' => 'submitReply',
                                                                    'class'=>'btn btn-primary float-right'
                                                                    ]);?>
                                                                <?= $this->Form->end() ?>
                                                            </div>
                                                        </div>
                                                        
                                                <!-- Tweet Utilities End -->
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <!-- Modal -->
                                        <div class="modal fade" id="retweet<?php echo $t['Tweet']['id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Preview</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            <div id="retweet-body<?php echo $t['Tweet']['id']?>" class="form-group-retweet">
                                                    <label class="sr-only" for="message">post</label>
                                                    <?php echo $this->Form->create('Tweet', [
                                                        'id' => 'retweet'. $t['Tweet']['id']
                                                        ]);
                                                        echo $this->Form->input('retweet_content', array(
                                                        'label' => false,
                                                        'type' => 'text',
                                                        'value' => '',
                                                        'rows' => '3',
                                                        'maxlength' => '140',
                                                        'class' => 'form-control',
                                                        'placeholder' => 'Please enter up to 140 characters',
                                                        'style' => 'margin-top: 10px'));
                                                        echo $this->Form->input('retweet_user_id',
                                                        ['id' => 'user_id',
                                                        'value' => $User['User']['id'],
                                                        'type' => 'hidden',
                                                        'label' => false]);
                                                        echo $this->Form->input('content',
                                                        ['id' => 'content',
                                                        'value' => h($t['Tweet']['content']),
                                                        'type' => 'hidden',
                                                        'label' => false]);
                                                        echo $this->Form->input('retweet_id',
                                                        ['id' => 'tweet_id',
                                                        'value' => $t['Tweet']['id'],
                                                        'type' => 'hidden',
                                                        'label' => false]);
                                                        echo $this->Form->input('retweet_picture',
                                                        ['id' => 'picture',
                                                        'value' => $t['Tweet']['picture'],
                                                        'type' => 'hidden',   
                                                        'label' => false]);
                                                        echo $this->Form->input('user_id',
                                                        ['id' => 'user_id',
                                                        'value' => $User['User']['id'],
                                                        'type' => 'hidden',
                                                        'label' => false]);
                                                    ?>
                                            </div>
                                            </div>
                                            <div class="card gedf-card">    
                                                <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <div class="mr-2">
                                                                <?= $this->Html->image("/img/users/" . $t['User']['image'], ['class' => 'rounded-circle', 'width' => '45']);?>
                                                            </div>
                                                            <div class="ml-2">
                                                                <div class="h5 m-0"><?php echo h($t['User']['username']); ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                <div>
                                                <?= $this->Html->image("/img/users/" . $t['Tweet']['picture'], ['class' => 'box', 'width' => '300', 'height' => '300']);?>
                                                </div>
                                                    <p class="card-text">
                                                        <?php echo h($t['Tweet']['content']); ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <div id="inputButton">
                                                        <?= $this->Form->input('Retweet',
                                                        ['type' => 'button',
                                                        'label' => false,
                                                        'id' => 'submitRetweet',
                                                        'class'=>'btn btn-primary float-right'
                                                        ]);?>
                                                    <?= $this->Form->end() ?>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    <!-- Modal End -->
                        <?php endforeach; ?>
                    </div>
                    <div class="tab-panel fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="row">
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Email</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p><?php echo $User['User']['email']; ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Birthday</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p><?php echo $User['User']['birthday']; ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p><?php echo $User['User']['location']; ?></p>
                                    </div>
                                </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Your Bio</label><br/>
                                <?php echo $User['User']['description']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>           
</div>

<script>
$(document).ready(function() {
    $('#follow').on('click', function() {
        var el = $(this);
        console.log(el);
        textNode = this.lastChild;
        console.log(textNode);
        textNode.nodeValue = (el.hasClass('btn btn-primary') ? 'Followed' : 'Follow');
        el.toggleClass('btn-primary');

                var a = <?php echo h($Other['User']['id'])?>;
                var b = <?php echo h($User['User']['id'])?>

                if (textNode.nodeValue == 'Followed') {
                    $.ajax({
                    type: 'POST',
                    url: "/microblog/users/follow",
                    data: {following_id: a, user_id: b},
                    
                    success: function(data,textStatus,xhr){
                    },
                    error: function(xhr,textStatus,error){
                    }
                    });
                    return false;
                } else if (textNode.nodeValue == 'Follow') {
                    $.ajax({
                    type: 'POST',
                    url: "/microblog/users/unfollow",
                    data: {following_id: a, user_id: b},
                    
                    success: function(data,textStatus,xhr){
                    },
                    error: function(xhr,textStatus,error){
                    }
                    });
                    return false;
                }
    });
    <?php foreach ($tweet as $t): ?>
    $('#like_post<?php echo $t['Tweet']['id']?>').on('click', function(event) {
            event.preventDefault();
            $('#like_post<?php echo $t['Tweet']['id']?>').each(function () {            
                var el = $(this);
                textNode = this.lastChild;
                console.log(textNode);
                el.find('span').toggleClass('fa-thumbs-up');
                textNode.nodeValue = (el.hasClass('likeChange') ? 'Liked' : 'Like');
                el.toggleClass('likeChange');

                var a = <?php echo h($User['User']['id'])?>;
                var b = <?php echo h($t['Tweet']['id'])?>

                if (textNode.nodeValue == 'Liked') {
                    $.ajax({
                    type: 'POST',
                    url: "like",
                    data: {user_id: a, tweet_id: b},
                    
                    success: function(data,textStatus,xhr){
                    },
                    error: function(xhr,textStatus,error){
                    }
                    });
                    return false;
                } else if (textNode.nodeValue == 'Like') {
                    $.ajax({
                    type: 'POST',
                    url: "unlike",
                    data: {user_id: a, tweet_id: b},
                    
                    success: function(response){
                    },
                    error: function(xhr,textStatus,error){
                    }
                    });
                    return false;
                }
            });
        });

        $('#formButton<?php echo $t['Tweet']['id']?>').on('click', function(event) {
            event.preventDefault();
            $('#like_post<?php echo $t['Tweet']['id']?>').each(function () {            
                $('#reply-body<?php echo $t['Tweet']['id']?>').slideToggle();
            });
        });
        $('#more<?php echo $t['Tweet']['id']?>').on('click', function(event) {
            event.preventDefault();
            $('#more<?php echo $t['Tweet']['id']?>').each(function () {            
                $('#replies<?php echo $t['Tweet']['id']?>').slideToggle('fast');
            });
        });
        $('#formRetweet<?php echo $t['Tweet']['id']?>').on('click', function(event) {
            event.preventDefault();
            $('#like_post<?php echo $t['Tweet']['id']?>').each(function () {            
                $('#retweet<?php echo $t['Tweet']['id']?>').modal('show');
            });
        });
    <?php endforeach; ?>
});
</script>
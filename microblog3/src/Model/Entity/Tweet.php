<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tweet Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $content
 * @property string $picture
 * @property int $retweet_id
 * @property int $retweet_user_id
 * @property int $retweet_user_content
 * @property int $retweet_user_picture
 * @property \Cake\I18n\FrozenDate $created
 * @property \Cake\I18n\FrozenDate $modified
 * @property string $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Retweet $retweet
 * @property \App\Model\Entity\RetweetUser $retweet_user
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Like[] $likes
 */
class Tweet extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'content' => true,
        'picture' => true,
        'retweet_id' => true,
        'retweet_user_id' => true,
        'retweet_content' => true,
        'retweet_picture' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'retweet' => true,
        'retweet_user' => true,
        'comments' => true,
        'likes' => true
    ];
}

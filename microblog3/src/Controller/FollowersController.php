<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\FollowersTable;
use Cake\ORM\TableRegistry;

/**
 * Followers Controller
 *
 * @property \App\Model\Table\FollowersTable $Followers
 *
 * @method \App\Model\Entity\Follower[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersController extends AppController
{

    public function followers() {
        $this->viewBuilder()->setLayout('main');
        $this->loadModel('Users');

        /**
         * followingUser
         * Check for users you've been following 
         * 
         */
        $followingUser = $this->Followers->find()
            ->contain([
                'Users' => [
                    'strategy' => 'select'
                ],
                'Follows' => [
                    'strategy' => 'select'
                ],
            ])
            ->where([
                'Followers.following_id' => $this->Auth->User('id'),
                'Followers.status' => 1
            ]);
        $this->set(compact('followingUser'));

        /**
         * followUser
         * Check for users that follows you
         * 
         */
        $followUser = $this->Followers->find()
            ->contain([
                'Users',
                'Follows'
            ])
            ->where([
                'Followers.user_id' => $this->Auth->User('id'),
                'Followers.status' => 1
            ]);
        $this->set(compact('followUser'));
    }

    /**
     * User Follow
     * 
     * 
     */
    public function follow() {
        //$this->request->onlyAllow('ajax');
        $this->autoRender = false;

        $post = $this->request->data;

        $find = $this->Followers->find()
            ->where([
                'Followers.following_id' => $post['following_id'],
                'Followers.user_id' => $post['user_id']
            ])
            ->first();

        if ($this->request->is('ajax')) {
            $followTable = TableRegistry::get('Followers');
            $followData = $followTable->newEntity();

            if(empty($find)) {
                $followData->following_id = $post['following_id'];
                $followData->user_id = $post['user_id'];
                $followData->status = 1;

            } else {
                $followData->id = $find->id;
                $followData->following_id = $post['following_id'];
                $followData->user_id = $post['user_id'];
                $followData->status = 1;
            }

            if ($followTable->save($followData)) {
                $message = [
                    'data_count' => $this->Followers->find()
                        ->where([
                            'Followers.following_id' => $post['following_id'],
                            'status' => 1
                        ])
                        ->count(),
                    'message' => 'Follow Successful',
                    'status' => 'success'
                ];
                $this->response->body(json_encode($message));
                return $this->response;

            } else {
                $message = [
                    'message' => 'Follow Failed',
                    'status' => 'success'
                ];
                $this->response->body(json_encode($message));
                return $this->response;

            }
        }
    }

    /**
     * User Unfollow
     * 
     * 
     */
    public function unfollow() {
        $this->loadModel('Follower');
        $this->autoRender = false;

        $post = $this->request->data;

        $find = $this->Followers->find()
            ->where([
                'Followers.following_id' => $post['following_id'],
                'Followers.user_id' => $post['user_id']
            ])
            ->first();

            if ($this->request->is('ajax')) {
                $followTable = TableRegistry::get('Followers');
                $followData = $followTable->newEntity();

                $followData->id = $find->id;
                $followData->following_id = $post['following_id'];
                $followData->user_id = $post['user_id'];
                $followData->status = 0;

                if ($followTable->save($followData)) {
                    $message = [
                        'data_count' => $this->Followers->find()
                            ->where([
                                'Followers.following_id' => $post['following_id'],
                                'status' => 1
                            ])
                            ->count(),
                        'message' => 'Unfollow Successful',
                        'status' => 'success'
                    ];
                    $this->response->body(json_encode($message));
                    return $this->response;

                } else {
                    $message = [
                        'message' => 'Unfollow Failed',
                        'status' => 'success'
                    ];
                    $this->response->body(json_encode($message));
                    return $this->response;
                }
            }
    }
}

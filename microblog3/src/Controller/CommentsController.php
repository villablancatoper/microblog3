<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 *
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
    /**
     * Saves Comments
     * 
     * 
     */
    public function save() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $comments = $this->Comments->newEntity();
            $data = $this->request->getData();

            if (!empty($data['comment_indicate'])) {
                if (empty($data['comment'])) {
                    $this->Flash->error(__('Please enter some comment before continuing'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
                }
            }

            if (isset($data['comment'])) {
                $comments = $this->Comments->patchEntity($comments, $data);

                if ($this->Comments->save($comments)) {
                    return $this->redirect($this->referer());
                }
                unset($data['comment']);
            }

        }
    }

    /**
     * Edit comments
     * 
     * 
     */
    public function edit() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $comments = $this->Comments->newEntity();
            $data = $this->request->getData();

            if (!empty($data['comment_indicate'])) {
                if (empty($data['comment'])) {
                    $this->Flash->error(__('Please enter some comment before continuing'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
                }
            }

            if (isset($data['comment'])) {
                $comment = $this->Comments->get($data['id']);
                $comments = $this->Comments->patchEntity($comment, $data);

                if ($this->Comments->save($comments)) {
                    return $this->redirect($this->referer());
                }
                unset($data['comment']);
            }

        }
    }

    /**
     * delete comment
     * 
     * 
     */
    public function delete($id) {
        $this->autoRender = false;

        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        $delete = $this->Comments->find()
            ->where([
                'Comments.id' => $id
            ])
            ->first();

        if (!empty($delete)){
            $tweetsTable = $this->Comments->get($id);

            if($this->Comments->delete($tweetsTable)){
                $this->Flash->success(__('Comment successfully deleted!'));
                return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
            }
        }
    }
}

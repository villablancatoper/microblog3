<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Search Results
     *
     *
     */
    public function searchResult()
    {
        $this->layout = 'main';
        $this->loadModel('Tweets');
        $this->loadModel('Users');
        $key = trim($this->request->query['search']);

        if (empty($key)) {
            $this->Flash->error(__('Invalid input'));
            return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
        }


        //component that finds user infirmation
        $user = $this->Users->find()
            ->where([
                'Users.username' => $this->Auth->user('username')
            ])
            ->first();
        $this->set(compact('user'));

        $searchUser = $this->Users->find('all', [
            'conditions' => [
                'OR' => [
                    'Users.username LIKE' => "%$key%",
                    'Users.name LIKE' => "%$key%"
                ]
            ]
        ]);
        $this->set('searchUser', $searchUser);


        $searchTweet = $this->Tweets->find('all', [
            'conditions' => [
                'OR' => [
                    'Tweets.content LIKE' => "%$key%",
                    'Tweets.retweet_content LIKE' => "%$key%",
                ] 
            ],
            'contain' => [
                'Users'
            ]
        ]);
        $this->set('searchTweet', $searchTweet);
    }
}

<div class="container">
    <div class="container-fluid gedf-wrapper">
        <div class="row">
            <div class="offset-1 col-md-10 gedf-main">
                <!-- Main Tweet Section-->
                    <div class="card gedf-card">
                        <div class="card-header">
                            <h4> List of followers </h4>
                            <p style="text-align: right">
                                <a href="<?= $url ?>users/dashboard">
                                    Return 
                                </a>
                            </p>
                            <hr>
                        </div>
                        <?php foreach ($followingUser as $f):?>
                            <div>
                                <div class="card border-light bg-white card proviewcard shadow-sm offset-1 col-md-9 offset-1">
                                <div class="card-header"></div>
                                <div class="card-body">
                                    <div class="col-lg-12 p-3 cardlist">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="row">
                                                        <div class="col-4 col-lg-3 col-xl-2">
                                                            <div class="row">
                                                            <?php if (!empty($f->follow->image)) : ?>
                                                                <?= $this->Html->image("/img/users/" . $f->follow->image, ['class' => '', 'width' => '70', 'height' => '80']);?>
                                                            <?php else: ?>
                                                                <?= $this->Html->image("/img/users/default_picture", ['class' => 'rounded-circle', 'width' => '45']);?>
                                                            <?php endif;?>
                                                            </div>
                                                        </div>
                                                        <div class="col-8 col-lg-9 col-xl-10">
                                                            <div class="d-block text-truncate mb-1">
                                                                <?php $id = $f->id ?>
                                                                <?php echo $this->Html->Link($f->follow->username, [
                                                                'label' => false,
                                                                'action' => 'other_user', $id
                                                                    ]
                                                                );
                                                                ?>
                                                            </div>
                                                            <div class="location d-block">
                                                                <span><?php echo $f->follow->email; ?> </span>
                                                            </div>
                                                            <div class="cartviewprice d-block">
                                                                <span>Location: <?php echo $f->follow->location; ?> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-4 col-lg-3 col-xl-2 p-0 qty">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 ml-lg-auto align-self-start mt-2 mt-lg-0">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                        <div class="card gedf-card">
                        <div class="card-header">
                            <h4> List of people you followed </h4>
                            <hr>
                        </div>
                        <?php foreach ($followUser as $flwUser):?>
                            <div>
                                <div class="card border-light bg-white card proviewcard shadow-sm offset-1 col-md-9 offset-1">
                                <div class="card-header"></div>
                                <div class="card-body">
                                    <div class="col-lg-12 p-3 cardlist">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="row">
                                                        <div class="col-4 col-lg-3 col-xl-2">
                                                            <div class="row">
                                                            <?php if (!empty($flwUser->user->image)) : ?>
                                                                <?= $this->Html->image("/img/users/" . $flwUser->user->image, ['class' => '', 'width' => '70', 'height' => '80']);?>
                                                            <?php else: ?>
                                                                <?= $this->Html->image("/img/users/default_picture", ['class' => 'rounded-circle', 'width' => '45']);?>
                                                            <?php endif;?>
                                                            </div>
                                                        </div>
                                                        <div class="col-8 col-lg-9 col-xl-10">
                                                            <div class="d-block text-truncate mb-1">
                                                                <?php $id = $flwUser->user->id ?>
                                                                <?php echo $this->Html->Link($flwUser->user->username, [
                                                                'label' => false,
                                                                'action' => 'other_user', $id
                                                                    ]
                                                                );
                                                                ?>
                                                            </div>
                                                            <div class="location d-block">
                                                                <span><?php echo h($flwUser->user->email); ?> </span>
                                                            </div>
                                                            <div class="cartviewprice d-block">
                                                                <span>Location: <?php echo h($flwUser->user->location); ?> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-4 col-lg-3 col-xl-2 p-0 qty">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 ml-lg-auto align-self-start mt-2 mt-lg-0">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
                <!-- Main Tweet Section End-->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
</script>
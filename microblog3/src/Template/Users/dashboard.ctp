<div class="container-main">
    <?= $this->Flash->render()?>
    <div class="container-fluid gedf-wrapper">
        <div class="row">
                <!-- User Details -->
                    <div class="col-md-3">
                        <div class="card-new" style="background-color: #30d7e3">
                            <div class="profile-userpic">
                                <?php if (!empty($user->image)) : ?>
                                    <?= $this->Html->image("/img/users/" . h($user->image), ['width' => '200px', 'heigth' => '200px', 'style' => 'margin-left: 20px']);?>
                                <?php else: ?>
                                    <?= $this->Html->image("/img/users/default_picture", ['class' => 'rounded-circle', 'width' => '45']);?>
                                <?php endif;?>
                            </div>
                            <div class="card-body"><h3>
                                @
                                <?php
                                    $id = h($user->id);
                                    echo $this->Html->Link($user->username, array(
                                    'label' => false,
                                    'action' => 'profile'
                                        )
                                    );
                                ?>
                                </h3>
                                <div class="h7 text-muted">Fullname : <?= h($user->name); ?></div>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="h6 text-muted">Followers</div>
                                    <div class="h5"><?= $isFollowed; ?></div>
                                </li>
                                <li class="list-group-item">
                                    <div class="h6 text-muted">Following</div>
                                    <div class="h5"><?= $isFollowing; ?></div>
                                </li>
                                <li class="list-group-item active" style="background-color: #80e4e3db">
                                    <a href="<?= $url ?>dashboard">
                                        <i class="fa fa-home"></i>
                                        Home
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="<?= $url ?>users/profile">
                                        <i class="fa fa-user"></i>
                                        Profile 
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="<?= $url ?>followers/followers">
                                        <i class="fa fa-users"></i>
                                        Followers 
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                <!-- User Details End -->
                <!-- User Tweets -->
                    <div class="col-md-6 gedf-main">
                        <!-- Main Tweet Section-->
                            <div class="card gedf-card">
                                <div class="card-header" style="background-color: rgb(245, 174, 174)">
                                    <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Make
                                                a publication</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                                            <div class="form-group">
                                                <label class="sr-only" for="message">post</label>
                                                <?= $this->Form->create('null', [
                                                    'id' => 'tweetSubmit',
                                                    'enctype' => 'multipart/form-data',
                                                    'url' => $this->Url->build('/tweets/save', true)
                                                ]);
                                                    echo $this->Form->control('user_id',
                                                    ['id' => 'user_id',
                                                    'value' => $user->id,
                                                    'type' => 'hidden',
                                                    'label' => false]);
                                                    echo $this->Form->textarea('content', [
                                                    'label' => false,
                                                    'type' => 'text',
                                                    'text' => '',
                                                    'rows' => '3',
                                                    'maxlength' => '140',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Please enter up to 140 characters',
                                                    'style' => 'margin-top: 10px']);
                                                ?>
                                                <br />
                                                    <h4><span> Or post a picture </span></h4>
                                                <br />
                                                <div>

                                                </div>
                                                <?=
                                                    $this->Form->control('picture', [
                                                        'id' => 'picValidate',
                                                        'label' => false,
                                                        'type' => 'file',
                                                        'accept' => '.jpg,.jpeg,.gif,.png'
                                                    ]);
                                                ?>
                                            </div>
                                            <div id="controlButton">
                                                <?= $this->Form->button('Submit', [
                                                    'label' => false,
                                                    'id' => 'submitPost',
                                                    'class' => 'btn btn-primary float-right',
                                                    ]);
                                                ?>
                                                <?= $this->Form->end() ?>
                                                <button id="submittedPost" style='display:none;'></button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="btn-toolbar justify-content-between">
                                    </div>
                                </div>
                            </div>
                        <!-- Main Tweet Section End-->
                        <!-- Replies and Other Functions-->
                            <?php foreach ($userTweet as $t): ?>
                                <br />
                                <!-- if card is retweeted -->
                                <?php if (!empty($t->retweet_id)) { ?>
                                    <div class="card gedf-card"> 
                                        <div class="card-header" style="background-color: rgb(245, 174, 174)">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="mr-2">
                                                        <?php if (!empty($t->retweeter->image)) : ?>
                                                            <?= $this->Html->image("/img/users/" . h($t->retweeter->image), ['class' => 'rounded-circle', 'width' => '45']);?>
                                                        <?php else: ?>
                                                            <?= $this->Html->image("/img/users/default_picture", ['class' => 'rounded-circle', 'width' => '45']);?>
                                                        <?php endif;?>
                                                        
                                                    </div>
                                                    <div class="ml-2">
                                                        <div class="h5 m-0">
                                                        <h4>
                                                        <?php if ($user->id == $t->retweeter->id) {
                                                            $id = h($t->user->id);
                                                            echo $this->Html->Link($t->retweeter->username, array(
                                                            'label' => false,
                                                            'action' => 'profile'
                                                            )
                                                        );
                                                        ?>
                                                        <?php } else { 
                                                            $id = h($t->retweeter->id);
                                                            echo $this->Html->Link($t->retweeter->username, array(
                                                        'label' => false,
                                                        'action' => '/other_user', $id
                                                            )
                                                        );
                                                        ?>
                                                        <?php } ?>
                                                        </h4> retweeted:
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                <?php if ($t->retweeter->id == $user->id) { ?>
                                                    <div class="dropdown">
                                                        <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fa fa-ellipsis-h"></i>
                                                        </button>
                                                        
                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                                        <?= $this->Form->postLink(
                                                            'Delete',
                                                            ['class' => 'dropdown-item', 'action' => 'delete', $t->id, 'type' => 'button'],
                                                            ['confirm' => 'Are you sure?']
                                                            );
                                                        ?>
                                                            <hr>
                                                            <button id="formRetweetEdit<?= $t->id?>" class="dropdown-item">
                                                                <span class="fa fa-edit"></span> Edit
                                                            </button>
                                                        </div>
                                                    </div>
                                                <?php } else { ?>
                                                <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div>
                                                <p class="card-text">
                                                    <?= h($t->retweet_content); ?>
                                                </p>
                                            </div>
                                            <hr>
                                                <div class="card gedf-card offset-1">
                                                    <div class="card-header" style="background-color: rgb(245, 174, 174)">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <div class="mr-2">
                                                                    <?php if (!empty($t->user->image)) : ?>
                                                                    <?= $this->Html->image("/img/users/" . $t->user->image, ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                    <?php else: ?>
                                                                        <?= $this->Html->image("/img/users/default_picture", ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                    <?php endif;?>
                                                                </div>
                                                                <div class="ml-2">
                                                                <h4>
                                                                <?php if ($user->id == $t->user->id) {
                                                                    $id = h($t->user->id);
                                                                    echo $this->Html->Link($t->user->username, array(
                                                                    'label' => false,
                                                                    'action' => 'profile'
                                                                    )
                                                                );
                                                                ?>
                                                                <?php } else { 
                                                                    $id = h($t->user->id);
                                                                    echo $this->Html->Link($t->user->username, array(
                                                                'label' => false,
                                                                'action' => '/other_user', $id
                                                                    )
                                                                );
                                                                ?>
                                                                <?php } ?>
                                                                </h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <p class="card-text">
                                                            <?php if (in_array($t->retweet_id , $tweetId)): ?>
                                                                <div>
                                                                    <?= h($t->content); ?>
                                                                </div>
                                                                <div>
                                                                    <?= $this->Html->image("/img/users/" . $t->retweet_picture, ['class' => 'box', 'width' => '300', 'height' => '300']);?>
                                                                </div>
                                                            <?php else :?>
                                                                <p> This content is no longer available </p>
                                                            <?php endif; ?>
                                                        </p>
                                                </div>
                                        </div>
                                        <div class="card-footer">
                                            <!-- Tweet Utilites -->
                                                    <p> Created: <?= $t->created ?> </p>
                                                    <?php $ctr = 0 ?>
                                                    <?php foreach ($isLiked as $il) : ?>
                                                        <?php if ($il == $t->id) {
                                                            $ctr++;
                                                        }
                                                        ?>
                                                    <?php endforeach; ?>
                                                    <span class="fa fa-thumbs-up" id="likeCount<?= $t->id?>"><?= $ctr; ?></span> 
                                                    <br />
                                                    <hr>
                                                    <?php if (!empty($t->comments)) :?>
                                                        <a href="#a" id="more<?= $t->id?>">View Replies</a>
                                                    <?php endif; ?>
                                                    <div id="replies<?= $t->id?>" class="form-group-replies" style="display: none">
                                                    <!-- Comment Section -->
                                                    <?php foreach ($commentTweet as $c): ?>
                                                        <div id="#a reply-header<?= $t->id?>">
                                                        <div class="card gedf-card">
                                                        <?php if ($t->id == $c->tweet_id) { ?>
                                                                    <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                                    <?php if ($t->user->id == $user->id) { ?>
                                                                        <div class="d-flex justify-content-between align-items-center">
                                                                            <div class="d-flex justify-content-between align-items-center">
                                                                                <div class="mr-2">
                                                                                    <?php if (!empty($c->user->image)) : ?>
                                                                                    <?= $this->Html->image("/img/users/" . $c->user->image, ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                                    <?php else: ?>
                                                                                        <?= $this->Html->image("/img/users/default_picture", ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                                    <?php endif;?>
                                                                                </div>
                                                                                <div class="ml-2">
                                                                                    <h4>
                                                                                    <?php if ($user->id == $c->user->id) {
                                                                                        $id = h($c->user->id);
                                                                                        echo $this->Html->Link($c->user->username, array(
                                                                                        'label' => false,
                                                                                        'action' => 'profile'
                                                                                        )
                                                                                    );
                                                                                    ?>
                                                                                    <?php } else { 
                                                                                        $id = h($c->user->id);
                                                                                        echo $this->Html->Link($c->user->username, array(
                                                                                    'label' => false,
                                                                                    'action' => '/other_user', $id
                                                                                        )
                                                                                    );
                                                                                    ?>
                                                                                    <?php } ?>
                                                                                    </h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="dropdown">
                                                                                        <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                            <i class="fa fa-ellipsis-h"></i>
                                                                                        </button>
                                                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                                                                            <?= $this->Form->postLink(
                                                                                                'Delete',
                                                                                                ['class' => 'dropdown-item', 'controller' => 'comments', 'action' => 'delete', $c->id, 'type' => 'button'],
                                                                                                ['confirm' => 'Are you sure?']
                                                                                                );
                                                                                            ?>
                                                                                            <!-- <button id="formCommentRetweetEdit<?= $c->id ?>" class="dropdown-item">
                                                                                                <span class="fa fa-edit"></span> Edit
                                                                                            </button> -->
                                                                                        </div>
                                                                                    </div>
                                                                                <?php } else { ?>
                                                                                <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <p class="card-text">
                                                                            <?= h($c->comment); ?>
                                                                        </p>
                                                                    </div>
                                                                    <hr>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                    <!-- Comment Section end -->
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <?php if (in_array($t->retweet_id , $tweetId)): ?>
                                                        <?php if (in_array($t->id , $isLiked)) :?>
                                                            <button id="liked_post-<?= $t->id?>" class="btn btn-outline-secondary likeChange" data-id="<?= $t->id?>" data-user="<?= $user->id ?>">
                                                                Liked
                                                            </button>
                                                        <?php else :?>
                                                            <button id="like_post-<?= $t->id?>" class="btn btn-outline-secondary likeChange" data-id="<?= $t->id?>" data-user="<?= $user->id ?>">
                                                                Like
                                                            </button>
                                                        <?php endif; ?>
                                                        <button id="formButton<?= $t->id?>" name="" class="btn btn-outline-secondary comment">
                                                        <span class="fa fa-comment"></span> Comment
                                                        </button>
                                                        <button id="formRetweet<?= $t->id?>" name="" class="btn btn-outline-secondary comment">
                                                        <span class="fa fa-retweet"></span> Retweet
                                                        </button>
                                                    <?php else :?>
                                                    <?php endif; ?>

                                                    <div id="reply-body<?= $t->id?>" class="form-group-reply" style="display: none">
                                                            <label class="sr-only" for="message">post</label>
                                                            <?= $this->Form->create('Comment', [
                                                                'url' => $this->Url->build('/comments/save', true)
                                                                ]);
                                                                echo $this->Form->control('comment', array(
                                                                'label' => false,
                                                                'type' => 'text',
                                                                'text' => '',
                                                                'rows' => '3',
                                                                'maxlength' => '140',
                                                                'class' => 'form-control',
                                                                'placeholder' => 'Please enter up to 140 characters',
                                                                'style' => 'margin-top: 10px'));
                                                                echo $this->Form->control('user_id',
                                                                ['id' => 'user_id',
                                                                'value' => $user->id,
                                                                'type' => 'hidden',
                                                                'label' => false]);
                                                                echo $this->Form->control('tweet_id',
                                                                ['id' => 'tweet_id',
                                                                'value' => $t->id,
                                                                'type' => 'hidden',
                                                                'label' => false]);
                                                                echo $this->Form->control('comment_indicate',
                                                                ['id' => 'tweet_id',
                                                                'value' => $t->id,
                                                                'type' => 'hidden',
                                                                'label' => false]);
                                                            ?>
                                                            <div id="controlButton">
                                                                <?= $this->Form->button('Reply',
                                                                [
                                                                'label' => false,
                                                                'id' => 'submitReply',
                                                                'class'=>'btn btn-primary float-right'
                                                                ]);?>
                                                            <?= $this->Form->end() ?>
                                                        </div>
                                                    </div>
                                            <!-- Tweet Utilities End -->
                                        </div>
                                        </div>
                                    </div>
                                <!-- if tweet is original -->
                                <?php } else { ?>
                                    <div class="card gedf-card">   
                                        <div class="card-header" style="background-color: rgb(245, 174, 174)">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="mr-2">
                                                        <?php if (!empty($t->user->image)) : ?>
                                                            <?= $this->Html->image("/img/users/" . $t->user->image, ['class' => 'rounded-circle', 'width' => '45']);?>
                                                            <?php else: ?>
                                                                <?= $this->Html->image("/img/users/default_picture", ['class' => 'rounded-circle', 'width' => '45']);?>
                                                            <?php endif;?>
                                                        </div>
                                                    <div class="ml-2">
                                                    <h4>
                                                        <?php if ($user->id == $t->user->id) {
                                                            $id = h($t->user->id);
                                                            echo $this->Html->Link($t->user->username, array(
                                                            'label' => false,
                                                            'action' => 'profile'
                                                            )
                                                        );
                                                        ?>
                                                        <?php } else { 
                                                            $id = h($t->user->id);
                                                            echo $this->Html->Link($t->user->username, array(
                                                        'label' => false,
                                                        'action' => '/other_user', $id
                                                            )
                                                        );
                                                        ?>
                                                        <?php } ?>
                                                    </h4>
                                                    </div>
                                                </div>
                                                <div>
                                                    <?php if ($t->user->id == $user->id) { ?>
                                                        <div class="dropdown">
                                                            <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-ellipsis-h"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                                                <?= $this->Form->postLink(
                                                                    'Delete',
                                                                    ['class' => 'dropdown-item', 'action' => 'delete', $t->id, 'type' => 'button'],
                                                                    ['confirm' => 'Are you sure?']
                                                                    );
                                                                ?>
                                                                <hr>
                                                                <button id="formEdit<?= $t->id?>" class="dropdown-item">
                                                                    <span class="fa fa-edit"></span> Edit
                                                                </button>
                                                            </div>
                                                        </div>
                                                    <?php } else { ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div>
                                                <p class="card-text">
                                                    <?= h($t->content); ?>
                                                </p>
                                            </div>
                                            <div>
                                                <?= $this->Html->image("/img/users/" . $t->picture, ['class' => '', 'width' => '300', 'height' => '300']);?>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <!-- Tweet Utilites -->
                                                    <p> Created: <?= h($t->created) ?> </p>
                                                    <?php $ctr = 0 ?>
                                                    <?php foreach ($isLiked as $il) : ?>
                                                        <?php if ($il == $t->id) {
                                                            $ctr++;
                                                        }
                                                        ?>
                                                    <?php endforeach; ?>
                                                    <span class="fa fa-thumbs-up" id="likeCount<?= $t->id?>"><?= $ctr?></span>
                                                    <br />
                                                    <hr>
                                                    <?php if (!empty($t->comments)) :?>
                                                        <a href="#a" id="more<?= $t->id?>">View Replies</a>
                                                    <?php endif; ?>
                                                    <div id="replies<?= $t->id?>" class="form-group-replies" style="display: none">
                                                    <?php foreach ($commentTweet as $c): ?>
                                                        <div id="#a reply-header<?= $t->id?>">
                                                            <div class="card gedf-card">
                                                                <?php if ($t->id == $c->tweet_id) { ?>
                                                                    <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                                        <div class="d-flex justify-content-between align-items-center">
                                                                            <div class="d-flex justify-content-between align-items-center">
                                                                                <div class="mr-2">
                                                                                <?php if (!empty($t->user->image)) : ?>
                                                                                    <?= $this->Html->image("/img/users/" . $c->user->image, ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                                <?php else: ?>
                                                                                    <?= $this->Html->image("/img/users/default_picture", ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                                <?php endif;?>
                                                                                </div>
                                                                                <div class="ml-2">
                                                                                <h4>
                                                                                <?php if ($user->id == $c->user->id) {
                                                                                    $id = h($c->user->id);
                                                                                    echo $this->Html->Link($c->user->username, array(
                                                                                    'label' => false,
                                                                                    'action' => 'profile'
                                                                                    )
                                                                                );
                                                                                ?>
                                                                                <?php } else { 
                                                                                    $id = h($c->user->id);
                                                                                    echo $this->Html->Link($c->user->username, array(
                                                                                'label' => false,
                                                                                'action' => '/other_user', $id
                                                                                    )
                                                                                );
                                                                                ?>
                                                                                <?php } ?>
                                                                                </h4>
                                                                                </div>
                                                                                <div>
                                                                            </div>
                                                                            </div>
                                                                                <?php if ($c->user->id == $user->id) { ?>
                                                                                    <div class="dropdown">
                                                                                        <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                            <i class="fa fa-ellipsis-h"></i>
                                                                                        </button>
                                                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                                                                            <?= $this->Form->postLink(
                                                                                                'Delete',
                                                                                                ['class' => 'dropdown-item', 'controller' => 'comments', 'action' => 'delete', $c->id, 'type' => 'button'],
                                                                                                ['confirm' => 'Are you sure?']
                                                                                                );
                                                                                            ?>
                                                                                            <!-- <button id="formCommentEdit<?= $c->id?>" class="dropdown-item">
                                                                                                <span class="fa fa-edit"></span> Edit
                                                                                            </button> -->
                                                                                        </div>
                                                                                    </div>
                                                                                <?php } else { ?>
                                                                                <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <p class="card-text">
                                                                            <?= h($c->comment); ?>
                                                                        </p>
                                                                    </div>
                                                                    <hr>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <?php if (in_array($t->id , $isLiked)) :?>
                                                        <button id="liked_post-<?= $t->id?>" class="btn btn-outline-secondary likeChange" data-id="<?= $t->id?>" data-user="<?= $user->id ?>">
                                                            Liked
                                                        </button>
                                                    <?php else :?>
                                                        <button id="like_post-<?= $t->id?>" class="btn btn-outline-secondary likeChange" data-id="<?= $t->id?>" data-user="<?= $user->id ?>">
                                                            Like
                                                        </button>
                                                    <?php endif; ?>
                                                    <button id="formButton<?= $t->id?>" name="" class="btn btn-outline-secondary comment">
                                                        <span class="fa fa-comment"></span> Comment 
                                                    </button>
                                                    <button id="formRetweet<?= $t->id?>" name="" class="btn btn-outline-secondary comment">
                                                        <span class="fa fa-retweet"></span> Retweet
                                                    </button>
                                                    <div id="reply-body<?= $t->id?>" class="form-group-reply" style="display: none">
                                                            <label class="sr-only" for="message">post</label>
                                                            <?= $this->Form->create('Comment', [
                                                                'url' => $this->Url->build('/comments/save', true)
                                                                ]);
                                                                echo $this->Form->textarea('comment', array(
                                                                'label' => false,
                                                                'type' => 'text',
                                                                'text' => '',
                                                                'rows' => '3',
                                                                'maxlength' => '140',
                                                                'class' => 'form-control',
                                                                'placeholder' => 'Please enter up to 140 characters',
                                                                'style' => 'margin-top: 10px'));
                                                                echo $this->Form->control('user_id',
                                                                ['id' => 'user_id',
                                                                'value' => $user->id,
                                                                'type' => 'hidden',
                                                                'label' => false]);
                                                                echo $this->Form->control('tweet_id',
                                                                ['id' => 'tweet_id',
                                                                'value' => $t->id,
                                                                'type' => 'hidden',
                                                                'label' => false]);
                                                                echo $this->Form->control('comment_indicate',
                                                                ['id' => 'tweet_id',
                                                                'value' => $t->id,
                                                                'type' => 'hidden',
                                                                'label' => false]);
                                                            ?>
                                                            <div id="controlButton">
                                                                <?= $this->Form->button('Reply',
                                                                [
                                                                'label' => false,
                                                                'id' => 'submitReply',
                                                                'class'=>'btn btn-primary float-right'
                                                                ]);?>
                                                            <?= $this->Form->end() ?>
                                                        </div>
                                                    </div>
                                                    
                                            <!-- Tweet Utilities End -->
                                        </div>
                                    </div>
                                <?php } ?>
                                <!-- Modal Retweet -->
                                    <div class="modal fade" id="retweet<?= $t->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Preview</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div id="retweet-body<?= $t->id?>" class="form-group-retweet">
                                                <?= $this->Form->create('post', [
                                                    'url' => $this->Url->build('/tweets/retweet', true)
                                                ]);
                                                    echo $this->Form->textarea('retweet_content', 
                                                    [
                                                        'label' => false,
                                                        'type' => 'text',
                                                        'rows' => '4',
                                                        'maxlength' => '140',
                                                        'class' => 'form-control',
                                                        'placeholder' => 'Please enter up to 140 characters',
                                                        'style' => 'margin-top: 10px'
                                                    ]);
                                                    echo $this->Form->control('retweet_user_id',
                                                    ['id' => 'retweet_user_id',
                                                    'value' => h($user->id),
                                                    'type' => 'hidden',
                                                    'label' => false]);
                                                    echo $this->Form->control('content',
                                                    ['id' => 'content',
                                                    'value' => h($t->content),
                                                    'type' => 'hidden',
                                                    'label' => false]);
                                                    echo $this->Form->control('retweet_id',
                                                    ['id' => 'tweet_id',
                                                    'value' => h($t->id),
                                                    'type' => 'hidden',
                                                    'label' => false]);
                                                    if (!empty($t->picture)) {
                                                        echo $this->Form->control('retweet_picture',
                                                        ['id' => 'retweet_picture',
                                                        'value' => h($t->picture),
                                                        'type' => 'hidden',   
                                                        'label' => false]);
                                                    } else {
                                                        echo $this->Form->control('retweet_picture',
                                                        ['id' => 'retweet_picture',
                                                        'value' => h($t->retweet_picture),
                                                        'type' => 'hidden',   
                                                        'label' => false]);
                                                    }
                                                    echo $this->Form->control('user_id',
                                                    ['id' => 'user_id',
                                                    'value' => h($t->user_id),
                                                    'type' => 'hidden',
                                                    'label' => false]);
                                                ?>
                                            </div>
                                        </div>
                                        <div class="card gedf-card">
                                            <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <?php if (!empty($t->user->image)) : ?>
                                                        <?= $this->Html->image("/img/users/" . $t->user->image, ['class' => 'rounded-circle', 'width' => '45']);?>
                                                        <?php else: ?>
                                                            <?= $this->Html->image("/img/users/default_picture", ['class' => 'rounded-circle', 'width' => '45']);?>
                                                        <?php endif;?>
                                                        <div class="ml-2">
                                                            <div class="h5 m-0"><?= h($t->user->username); ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div>
                                                    <p class="card-text">
                                                        <?= h($t->content); ?>
                                                    </p>
                                                </div>
                                                <?php if(!empty($t->retweet_picture)): ?>
                                                    <div>
                                                        <?= $this->Html->image("/img/users/" . $t->retweet_picture, ['class' => '', 'width' => '300', 'height' => '300']);?>
                                                    </div>
                                                <?php else : ?>
                                                    <div>
                                                        <?= $this->Html->image("/img/users/" . $t->picture, ['class' => '', 'width' => '300', 'height' => '300']);?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <div id="controlButton">
                                                    <?= $this->Form->button('Retweet',
                                                    [
                                                    'label' => false,
                                                    'id' => 'submitRetweet',
                                                    'class'=>'btn btn-primary float-right'
                                                    ]);?>
                                                <?= $this->Form->end() ?>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                <!-- Modal End -->
                                <!-- Modal Edit -->
                                    <div class="modal fade" id="edit<?= $t->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit Post</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                        <div id="retweet-body<?= $t->id?>" class="form-group-retweet">
                                                <?= $this->Form->create('null', [
                                                    'url' => $this->Url->build('/tweets/save', true)
                                                ]);
                                                echo $this->Form->textarea('content', 
                                                [
                                                    'label' => false,
                                                    'type' => 'text',
                                                    'rows' => '4',
                                                    'value' => $t->content,
                                                    'maxlength' => '140',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Please enter up to 140 characters',
                                                    'style' => 'margin-top: 10px'
                                                ]);
                                                echo $this->Form->control('retweet_user_id',
                                                ['id' => 'retweet_user_id',
                                                'value' => h($user->id),
                                                'type' => 'hidden',
                                                'label' => false]);
                                                echo $this->Form->control('edit_indicator',
                                                ['id' => 'retweet_user_id',
                                                'value' => h($t->id),
                                                'type' => 'hidden',
                                                'label' => false]);
                                            ?>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <div id="controlButton">
                                                    <?= $this->Form->button('Edit',
                                                    [
                                                    'label' => false,
                                                    'id' => 'submitEdit',
                                                    'class'=>'btn btn-primary float-right'
                                                    ]);?>
                                                <?= $this->Form->end() ?>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                <!-- Modal Edit End -->
                                <!-- Modal Edit -->
                                    <div class="modal fade" id="retweet-edit<?= $t->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit Post</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                        <div id="retweet-body<?= $t->id?>" class="form-group-retweet">
                                                <?= $this->Form->create('null', [
                                                    'url' => $this->Url->build('/tweets/save', true)
                                                ]);
                                                echo $this->Form->textarea('retweet_content', 
                                                [
                                                    'label' => false,
                                                    'type' => 'text',
                                                    'rows' => '4',
                                                    'value' => $t->retweet_content,
                                                    'maxlength' => '140',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Please enter up to 140 characters',
                                                    'style' => 'margin-top: 10px'
                                                ]);
                                                echo $this->Form->control('retweet_user_id',
                                                ['id' => 'retweet_user_id',
                                                'value' => h($user->id),
                                                'type' => 'hidden',
                                                'label' => false]);
                                                echo $this->Form->control('edit_indicator',
                                                ['id' => 'retweet_user_id',
                                                'value' => h($t->id),
                                                'type' => 'hidden',
                                                'label' => false]);
                                            ?>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <div id="controlButton">
                                                    <?= $this->Form->button('Edit',
                                                    [
                                                    'label' => false,
                                                    'id' => 'submitEdit',
                                                    'class'=>'btn btn-primary float-right'
                                                    ]);?>
                                                <?= $this->Form->end() ?>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                <!-- Modal Edit End -->
                                            <!-- Modal Comment Edit -->
                                                <div class="modal fade" id="comment-edit<?= $t->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Post</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                            <div id="comment-body<?= $t->id?>" class="form-group-commnet">
                                                                    <?= $this->Form->create('null', [
                                                                        'url' => $this->Url->build('/comments/edit', true)
                                                                    ]);
                                                                    echo $this->Form->textarea('comment', 
                                                                    [
                                                                        'label' => false,
                                                                        'type' => 'text',
                                                                        'rows' => '4',
                                                                        'value' => $c->comment,
                                                                        'maxlength' => '140',
                                                                        'class' => 'form-control',
                                                                        'placeholder' => 'Please enter up to 140 characters',
                                                                        'style' => 'margin-top: 10px'
                                                                    ]);
                                                                    echo $this->Form->control('id',
                                                                    ['id' => 'id',
                                                                    'value' => h($c->id),
                                                                    'type' => 'hidden',
                                                                    'label' => false]);
                                                                    echo $this->Form->control('user_id',
                                                                    ['id' => 'user_id',
                                                                    'value' => h($user->id),
                                                                    'type' => 'hidden',
                                                                    'label' => false]);
                                                                    echo $this->Form->control('tweet_id',
                                                                    ['id' => 'tweet_id',
                                                                    'value' => h($t->id),
                                                                    'type' => 'hidden',
                                                                    'label' => false]);
                                                                ?>
                                                            </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <div id="controlButton">
                                                                <?= $this->Form->button('Edit',
                                                                [
                                                                'label' => false,
                                                                'id' => 'submitEdit',
                                                                'class'=>'btn btn-primary float-right'
                                                                ]);?>
                                                            <?= $this->Form->end() ?>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            <!-- Modal Comment Edit End -->
                                            <!-- Modal Comment Retweet Edit -->
                                                <div class="modal fade" id="comment-retweet-edit<?= $t->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Post</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                                <div id="retweet-body<?= $t->id?>" class="form-group-retweet">
                                                                        <?= $this->Form->create('null', [
                                                                            'url' => $this->Url->build('/comments/edit', true)
                                                                        ]);
                                                                        echo $this->Form->textarea('comment', 
                                                                        [
                                                                            'label' => false,
                                                                            'type' => 'text',
                                                                            'rows' => '4',
                                                                            'value' => $c->comment,
                                                                            'maxlength' => '140',
                                                                            'class' => 'form-control',
                                                                            'placeholder' => 'Please enter up to 140 characters',
                                                                            'style' => 'margin-top: 10px'
                                                                        ]);
                                                                        echo $this->Form->control('id',
                                                                        ['id' => 'id',
                                                                        'value' => h($c->id),
                                                                        'type' => 'hidden',
                                                                        'label' => false]);
                                                                        echo $this->Form->control('user_id',
                                                                        ['id' => 'user_id',
                                                                        'value' => h($user->id),
                                                                        'type' => 'hidden',
                                                                        'label' => false]);
                                                                        echo $this->Form->control('tweet_id',
                                                                        ['id' => 'tweet_id',
                                                                        'value' => h($t->id),
                                                                        'type' => 'hidden',
                                                                        'label' => false]);
                                                                    ?>
                                                                </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <div id="controlButton">
                                                                <?= $this->Form->button('Edit',
                                                                [
                                                                'label' => false,
                                                                'id' => 'submitEdit',
                                                                'class'=>'btn btn-primary float-right'
                                                                ]);?>
                                                            <?= $this->Form->end() ?>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            <!-- Modal Comment Retweet Edit End -->
                            <?php endforeach; ?>
                        <!-- Replies and Other Functions End-->
                        <br />
                        <div class="card">
                            <nav aria-label="Page navigation example">
                                <div class="pagination pagination-large">
                                    <ul class="pagination">
                                        <?php
                                            if ($this->Paginator->counter($this->Paginator->counter(['format' => __('{{count}}')])) > 5) {
                                                echo $this->Paginator->prev(__('<< prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                                                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                                                echo $this->Paginator->next(__('next>>'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                                            } else {
                                            }
                                        ?>
                                    </ul>
                                </div>
                                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                            </nav>
                        </div>
                    </div>
                <!-- User Tweets End -->
                <!-- User Suggestions -->
                    <div class="col-md-3">
                        <div class="card gedf-card"> 
                        <div class="card-header" style="background-color: rgb(245, 174, 174)">
                            <h3> Find Friends </h3>
                        </div>
                        <div class="card-body">
                            <?php foreach ($suggestUser as $search): ?>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <?php if (!empty($search->image)) : ?>
                                        <?= $this->Html->image("/img/users/" . $search->image, ['class' => 'rounded-circle', 'width' => '45']);?>
                                    <?php else: ?>
                                        <?= $this->Html->image("/img/users/default_picture", ['class' => 'rounded-circle', 'width' => '45']);?>
                                    <?php endif;?>
                                     
                                        <?php
                                            $id = h($search->id);
                                            echo $this->Html->Link($search->name, array(
                                            'label' => false,
                                            'action' => '/other_user', $id
                                            )
                                        );
                                    ?>
                                </li>
                            </ul>
                                <br />
                            <?php endforeach; ?>
                        </div>
                    </div>
                <!-- User Suggestions End -->
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
    <?php foreach ($userTweet as $t): ?>
        $('#formButton<?= $t->id?>').on('click', function() {
            $('#like_post-<?= $t->id?>').each(function () {
                $('#reply-body<?= $t->id?>').slideToggle('fast');
            });
        });
        $('#formButton<?= $t->id?>').on('click', function() {
            $('#liked_post-<?= $t->id?>').each(function () {
                $('#reply-body<?= $t->id?>').slideToggle('fast');
            });
        });
        $('#more<?= $t->id?>').on('click', function(event) {
            event.preventDefault();
            $('#more<?= $t->id?>').each(function () {
                $('#replies<?= $t->id?>').slideToggle('fast');
            });
        });
        $('#formRetweet<?= $t->id?>').on('click', function(event) {
            event.preventDefault();
            $('#like_post-<?= $t->id?>').each(function () {
                $('#retweet<?= $t->id?>').modal('show');
            });
        });
        $('#formRetweet<?= $t->id?>').on('click', function(event) {
            event.preventDefault();
            $('#liked_post-<?= $t->id?>').each(function () {
                $('#retweet<?= $t->id?>').modal('show');
            });
        });
        $('#formEdit<?= $t->id?>').on('click', function(event) {
            event.preventDefault();
            $('#like_post-<?= $t->id?>').each(function () {
                $('#edit<?= $t->id?>').modal('show');
            });
        });
        $('#formEdit<?= $t->id?>').on('click', function(event) {
            event.preventDefault();
            $('#liked_post-<?= $t->id?>').each(function () {
                $('#edit<?= $t->id?>').modal('show');
            });
        });
        $('#formRetweetEdit<?= $t->id?>').on('click', function(event) {
            event.preventDefault();
            $('#like_post-<?= $t->id?>').each(function () {
                $('#retweet-edit<?= $t->id?>').modal('show');
            });
        });
        <?php endforeach; ?>

        <?php foreach ($commentTweet as $c): ?>
        $('#formCommentEdit<?= $c->id?>').on('click', function(event) {
            event.preventDefault();
            $('#like_post-<?= $t->id?>').each(function () {
                $('#comment-edit<?= $t->id?>').modal('show');
            });
        });
        $('#formCommentRetweetEdit<?= $c->id?>').on('click', function(event) {
            event.preventDefault();
            $('#like_post-<?= $c->id?>').each(function () {
                $('#comment-retweet-edit<?= $t->id?>').modal('show');
            });
        });
        $('#formCommentEdit<?= $c->id?>').on('click', function(event) {
            event.preventDefault();
            $('#liked_post-<?= $t->id?>').each(function () {
                $('#comment-edit<?= $t->id?>').modal('show');
            });
        });
        $('#formCommentRetweetEdit<?= $c->id?>').on('click', function(event) {
            event.preventDefault();
            $('#liked_post-<?= $t->id?>').each(function () {
                $('#comment-retweet-edit<?= $t->id?>').modal('show');
            });
        });
        <?php endforeach; ?>

        $('#submitPost').click(function() { // bind click handler to both button
            $(this).hide(); // hide the clicked button
        });

        $('#submitPicture').click(function() { // bind click handler to both button
            $(this).hide(); // hide the clicked button
        });

        $('#submitReply').click(function() { // bind click handler to both button
            $(this).hide(); // hide the clicked button
        });

        $('#submitRetweet').click(function() { // bind click handler to both button
            $(this).hide(); // hide the clicked button
        });

        $('#submitEdit').click(function() { // bind click handler to both button
            $(this).hide(); // hide the clicked button
        });



        $('body').delegate('.likeChange','click', function() {
            let id = $(this).data('id');
            let user_id = $(this).data('user');
            
            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: "/like",
                dataType: 'json',
                data: {
                    user_id: user_id, 
                    tweet_id: id
                },
                success: function (response) {
                    if ($('#like_post-'+id).text == 'Like') {
                        $('#likeCount'+id).text(response['data_count']);
                        $('#like_post-'+id).text('Liked');
                        $('#liked_post-'+id).text('Liked');
                    } else {
                        $('#likeCount'+id).text(response['data_count']);
                        $('#like_post-'+id).text('Like');
                        $('#liked_post-'+id).text('Like');
                    }
                }
            });
        });

        $(document).on("click", "#delete", function(e) {
            e.preventDefault();
            var delete_id = $(this).attr('data-id');

            $.ajax({
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                type: 'POST',
                url: "/tweets/delete/" + delete_id,
                data: {
                    id: delete_id
                },
                success: function () {
                }
            });
        });

        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('.blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
            }
        };

        $("#picture").change(function() {
            readURL(this);
        });

        $("#tweetSubmit").submit( function(submitEvent) {
            var filename = $("#picValidate").val();

            if (filename) {
                var extension = filename.replace(/^.*\./, '');
                var allowedExtensions = /(jpg|jpeg|png|gif)$/i;
                extension = extension.toLowerCase();

                if (!allowedExtensions.exec(extension)) {
                    alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
                    submitEvent.preventDefault();
                    window.location.replace('/users/dashboard');
                }
            }
        });
    });
</script>

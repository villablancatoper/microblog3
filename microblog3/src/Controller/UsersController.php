<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Component\UsersComponent;
use Cake\Utility\Security;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Exception\NotFoundException;
use Cake\Routing\Router;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public $paginate = [
        'limit' => 5
    ];

    /**
     * Initialization
     * 
     * 
     */
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('main');
        $this->Auth->allow(['logout', 'register']);

        $this->loadComponent('Paginator');

        $this->loadModel('Followers');
        $this->loadModel('Likes');
        $this->loadModel('Tweets');
        $this->loadModel('Comments');

        $registry = new ComponentRegistry();
        $this->userComp = new UsersComponent($registry);
    }

    /**
     * Login Function
     * 
     * 
     */
    public function login()
    {
        $this->viewBuilder()->setLayout('default');


        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if($user['status'] == 1) {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                $this->Flash->error(__('Account not yet activated'));
                }
            } else {
            $this->Flash->error(__('Your username or password is incorrect.'));
            }
        }
    }

    /**
     * Logout Function
     * 
     * 
     */
    public function logout()
    {
        $this->Flash->success(__('You are now logged out.'));
        return $this->redirect($this->Auth->logout());
    }

    public function activate($activation_key) {
        $this->autoRender = false;


        $user = $this->Users->find()
            ->where([
                'Users.activation' => $activation_key
            ])
            ->first();

        $id = $user->id;


        if (!$user) {
            throw new NotFoundException();
        }


        if (isset($user->status) && $user->status !== 1 ) {
            $verify = $this->Users->find()
                ->where([
                    'Users.activation' => $activation_key
                ])
                ->first();

            if ($verify) {
                $usersTable= TableRegistry::get('Users');
                $userTable = $usersTable->get($id);
                $userTable->status = 1;

                if($this->Users->save($userTable)){
                    $this->Flash->success(__('Account successfully verified!'));
                    $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
                }
            }
        } else {
            $this->Flash->error(__('Account was already verified!'));
        }
    }

    public function register() 
    {
        $this->viewBuilder()->setLayout('default');
        $user = $this->Users->newEntity();

        if ($this->request->is('post')) { 
            $data = $this->request->getData();
            $data['activation'] =  Security::hash(Security::randomBytes(32));
            $user = $this->Users->patchEntity($user, $data);

            if ($this->Users->save($user)) {
                $subject = "Account Activation link send on your email";
                $name = $user->name;
                $to = trim($user->email);
                //data ready for activation

                $Email = new Email();
                $Email->transport('mailjet');
                $Email->emailFormat('html');
                $Email->from(['toperyah@gmail.com' => 'Microblog']);
                $Email->to($to);
                $Email->subject($subject);

                $activationUrl = $_SERVER['HTTP_HOST'] . "/users/activate/" . $data['activation']  ;

                $message = "Dear <span style='color:#666666'>" . $name . "</span>,<br/><br/>";
                $message .= "Your account has been created successfully by Administrator.<br/>";
                $message .= "Please find the below details of your account: <br/><br/>";
                $message .= "<b>Full Name:</b> " . $user['name'] . "<br/>";
                $message .= "<b>Email Address:</b> " . $user['email'] . "<br/>";
                $message .= "<b>Username:</b> " . $user['username'] . "<br/>";
            
                $message .= "<b>Activate your account by clicking on the below url:</b> <br/>";
                $message .= "<a href='$activationUrl'>$activationUrl</a><br/><br/>";
                $message .= "<br/>Thanks, <br/>Support Team";
                $Email->send($message);


                $this->Flash->success(__('User Registration Complete, Awaiting Verification.'));

                return $this->redirect(['action' => 'register']);
            }

            $this->Flash->error(__('User Registration could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }


    /**
     * User Dashboard
     * 
     * 
     * 
     */
    public function dashboard() 
    {
        /**
         *  finds user information
         * 
         * 
         */
        $user = $this->Users->find()
            ->where([
                'Users.username' => $this->Auth->user('username')
            ])
            ->first();
        $this->set(compact('user'));


        /**
         * Follower Information
         * 
         * 
         */
        $Follower = $this->Followers->find()
            ->where([
                'Followers.user_id' => $this->Auth->User('id'),
                'Followers.status' => 1
            ])
            ->all();
        $this->set(compact('Follower'));


        /**
         * getFollowers
         * Get follower of current user
         * 
         */
        $getFollowers = $this->Followers->find()
            ->where([
                'Followers.user_id' => $this->Auth->User('id'),
                'Followers.status' => 1
            ])
            ->extract('following_id')
            ->toArray();

        $getFollowers[] = $this->Auth->User('id');


        /**
         * tweetId
         * Gets tweet id
         * 
         */
        $tweetId = $this->Tweets->find()
            ->extract('id')
            ->toArray();
        $this->set(compact('tweetId'));



        /**
         * If current tweet is liked by user
         * 
         * 
         */
        $isLiked = $this->Likes->find()
            ->where([
                'Likes.user_id' => $this->Auth->User('id'),
                'Likes.is_liked' => 1
            ])
            ->extract('tweet_id')
            ->toArray();
        $this->set(compact('isLiked'));



        /**
         * User Tweet
         * Generates Both User information and Tweets
         * 
         */
        $userTweet = $this->Tweets->find()
            ->order([
                'Tweets.created' => 'DESC'
            ])
            ->contain([
                'Users',
                'Retweeter',
                'Comments'
            ])
            ->where([
                'Tweets.user_id IN' => $getFollowers,
                'Tweets.deleted' => 0
            ]);
            // pr(count($userTweet->toArray())); die;
        $this->set('userTweet', $this->paginate($userTweet));


        /**
         * Comments
         * Shows Comments of the tweet
         *
         */
        $commentTweet = $this->Comments->find()
        ->contain([
            'Users',
            'Tweets'
        ])
        ->all();
        $this->set(compact('commentTweet'));


        /**
         * User Following
         * 
         * 
         */
        $isFollowed = $this->Followers->find()
        ->where([
            'Followers.following_id' => $this->Auth->User('id'),
            'Followers.status' => 1
        ])
        ->count();
        $this->set(compact('isFollowed'));

        /**
         * User Followers
         * 
         * 
         */
        $isFollowing = $this->Followers->find()
            ->where([
                'Followers.user_id' => $this->Auth->User('id'),
                'Followers.status' => 1
            ])
            ->count();
        $this->set(compact('isFollowing'));

        /**
         * suggestUser
         * this suggests the users to be shown for each
         * 
         */
        $suggestUser = $this->Users->find()
            ->order([
                'rand()'
            ])
            ->where([
                'Users.id NOT IN' => $getFollowers,
                'Users.username !=' => $this->Auth->user('username')
            ])
            ->limit(5);
        $this->set(compact('suggestUser'));
    }

    /**
     * Edit user settings
     * 
     * 
     * 
     */
    public function editUser($id = null) 
    {

        $users = $this->Users->find()
            ->where([
                'Users.username' => $this->Auth->user('username')
            ])
            ->first();

        $id = $users->id;

        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }
        $find = $this->Users->findById($id);


        if (!$find) {
            throw new NotFoundException(__('Invalid post'));
        }


        $users = $this->Users->get($id);
        $data = $this->request->getData();
        $dataNew[] = $this->request->getData();


        if ($this->request->is(['post', 'put'])) {
            //pr($this->request->getData()); die;
            $filename = '';

            $picture = $this->userComp->uploadPhoto(
                collection($dataNew)
            );

            if ($picture === 'extension') {
                $this->Flash->error(__('Invalid Extension'));
                return $this->redirect(['controller' => 'Users', 'action' => 'editUser']);
            }


            if ($picture === 'path') {
                $this->Flash->error(__('Something went wrong with the path, please try again'));
                return $this->redirect(['controller' => 'Users', 'action' => 'editUser']);
            }


            if ($picture === 'size') {
                $this->Flash->error(__('Image size too large, upload only 8MB or less'));
                return $this->redirect(['controller' => 'Users', 'action' => 'editUser']);
            }


            if ($picture === null) {
                unset($data['image']);
            } else {
                $data['image'] = $picture;
            }

            if (empty($data['password'])) {
                unset($data['password']);
            }

            if (!empty($data['image'])) {
                $users = $this->Users->patchEntity($users, $data);

                if ($this->Users->save($users)) {
                    $this->Flash->success(__('Profile picture updated.'));
                    return $this->redirect(['action' => 'profile']);
                }
            }

            $users = $this->Users->patchEntity($users, $data);

        
            if ($this->Users->save($users)) {
                    $this->Flash->success(__('Your credentials has been updated.'));
                    return $this->redirect(['action' => 'profile']);
                }
            $this->Flash->error(__('Unable to update your credentials.'));
            }

        $this->set(compact('users', 'error'));
    }

    /**
     * Profile Page/ User side
     * 
     * 
     * 
     */
    public function profile() 
    {

        //finds user infirmation
        $user = $this->Users->find()
            ->where([
                'Users.username' => $this->Auth->user('username')
            ])
            ->first();
        $this->set(compact('user'));


        /**
         * User Tweet
         * -Generates Both User information and Tweets
         * 
         */
        $tweet = $this->Tweets->find()
            ->order([
                'Tweets.created' => 'DESC'
            ])
            ->contain([
                'Users',
                'Retweeter',
                'Comments'
            ])
            ->where([
                'Tweets.user_id' => $this->Auth->user('id'),
                'Tweets.deleted' => 0
            ]);
        $this->set('tweet', $this->paginate($tweet));

        /**
         * tweetId
         * Gets tweet id
         * 
         */
        $tweetId = $this->Tweets->find()
            ->extract('id')
            ->toArray();
        $this->set(compact('tweetId'));


        /**
         * Comments
         * Shows Comments of the tweet
         *
         */
        $commentTweet = $this->Comments->find()
        ->contain([
            'Users',
            'Tweets'
        ])
        ->all();
        $this->set(compact('commentTweet'));


        /**
         * count
         * get current like count
         *
         */
        $count = $this->Likes->find('all', array(
            'conditions' => [
                'OR' => [
                    ['Likes.is_liked' => 1]
                ]
            ]
        ));
        $this->set(compact('count'));


        /**
         * isLiked
         * Checks if the tweet is liked
         *
         */
        $isLiked = $this->Likes->find()
            ->where([
                'Likes.user_id' => $this->Auth->User('id'),
                'Likes.is_liked' => 1
            ])
            ->extract('tweet_id')
            ->toArray();
        $this->set(compact('isLiked'));



        $isFollowed = $this->Followers->find()
        ->where([
            'Followers.following_id' => $this->Auth->User('id'),
            'Followers.status' => 1
        ])
        ->count();
        $this->set(compact('isFollowed'));


        /**
         * User Followers
         * 
         * 
         */
        $isFollowing = $this->Followers->find()
            ->where([
                'Followers.user_id' => $this->Auth->User('id'),
                'Followers.status' => 1
            ])
            ->count();
        $this->set(compact('isFollowing'));
    }

    public function otherUser($id) {

        $cc = $this->Users->find()
        ->where([
            'Users.id' => $id
        ])
        ->first();
        if (empty($cc)) throw new NotFoundException("User Not Found", 1);


        /**
         *  finds user information
         * 
         * 
         */
        $user = $this->Users->find()
            ->where([
                'Users.username' => $this->Auth->User('username')
            ])
            ->first();
        $this->set(compact('user'));


        /**
         * Other user
         * 
         * 
         */
        $other = $this->Users->find('all', [
            'conditions' => [
                'Users.id' => $id
            ]
        ]);
        $other = $other->first();
        $this->set(compact('other'));


        $count = $this->Likes->find()
            ->where([
                'Likes.is_liked' => 1
            ]);
        $this->set(compact('count'));


        /**
         * isFollowed
         * checks if the user is followed
         *
         */
        $isFollowed = $this->Followers->find()
        ->where([
            'Followers.following_id' => $id,
            'Followers.status' => 1
        ])
        ->count();
        $this->set(compact('isFollowed'));


        /**
         * User Followers
         * 
         * 
         */
        $isFollowing = $this->Followers->find()
            ->where([
                'Followers.user_id' => $id,
                'Followers.status' => 1
            ])
            ->count();
        $this->set(compact('isFollowing'));


        /**
         * Comments
         * Shows Comments of the tweet
         *
         */
        $commentTweet = $this->Comments->find()
        ->contain([
            'Users',
            'Tweets'
        ])
        ->all();
        $this->set(compact('commentTweet'));
        
        $findFollowed = $this->Followers->find('all', array(
            'conditions' => [
                'AND' => [
                    ['Followers.following_id' => $id],
                    ['Followers.user_id' => $this->Auth->User('id')],
                    // ['Follower.status' => 1]
                ]
            ]
        ))->first();
        $this->set(compact('findFollowed'));

        /**
         * User Tweet
         * -Generates Both User information and Tweets
         * 
         */
        $tweet = $this->Tweets->find()
            ->order([
                'Tweets.created' => 'DESC'
            ])
            ->contain([
                'Users',
                'Retweeter',
                'Comments'
            ])
            ->where([
                'Tweets.user_id' => $id,
                'Tweets.deleted' => 0
            ]);
        $this->set('tweet', $this->paginate($tweet));

        /**
         * tweetId
         * Gets tweet id
         * 
         */
        $tweetId = $this->Tweets->find()
            ->extract('id')
            ->toArray();
        $this->set(compact('tweetId'));

        /**
         * If current tweet is liked by user
         * 
         * 
         */
        $isLiked = $this->Likes->find()
            ->where([
                'Likes.user_id' => $this->Auth->User('id'),
                'Likes.is_liked' => 1
            ])
            ->extract('tweet_id')
            ->toArray();
            

        $this->set(compact('isLiked'));
    }

    public function otherUserFollower($id) {
        $this->loadModel('Followers');

        $followingUser = $this->Followers->find('all', [
            'conditions' => [
                'Followers.following_id' => $id,
                'Followers.status' => 1
            ]
        ]);

        $this->set(compact('followingUser'));


        $followUser = $this->Followers->find('all', array(
            'conditions' => [
                'AND' => [
                    'Followers.user_id' => $id,
                    'Followers.status' => 1
                ]
            ]
            ));
        
        $this->set(compact('followUser'));
    }
}

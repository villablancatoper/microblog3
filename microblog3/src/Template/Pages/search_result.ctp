<div class="container emp-profile">
<?php $this->Flash->render()?>
    <div class="row">
        <div class="container-fluid well">
                <?php foreach ($searchUser as $su): ?>
                    <div class="card border-light bg-white card proviewcard shadow-sm offset-1 col-md-9 offset-1">
                        <div class="card-header"><h3>User</h3>
                        </div>
                        <div class="card-body">
                            <div class="col-lg-12 p-3 cardlist">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="col-4 col-lg-3 col-xl-2">
                                                    <div class="row">
                                                    <?php if (!empty($su->image)) : ?>
                                                        <?= $this->Html->image("/img/users/" . $su->image, ['class' => '', 'width' => '80', 'height' => '80']);?>
                                                    <?php else: ?>
                                                        <?= $this->Html->image("/img/users/default_picture", ['class' => '', 'width' => '80', 'height' => '80']);?>
                                                    <?php endif;?>
                                                    </div>
                                                </div>
                                                <div class="col-8 col-lg-9 col-xl-10">
                                                    <div class="d-block text-truncate mb-1">
                                                        @
                                                        <?php if ($user->id == $su->id) {
                                                            $id = h($su->id);
                                                            echo $this->Html->Link($su->username, array(
                                                            'label' => false,
                                                            'controller' => 'Users',
                                                            'action' => 'profile',
                                                            'escape' => false
                                                            )
                                                        );
                                                        ?>
                                                        <?php } else { 
                                                            $id = h($su->id);
                                                            echo $this->Html->Link($su->username, array(
                                                            'label' => false,
                                                            'controller' => 'Users',
                                                            'action' => 'other_user', $id,
                                                            'escape' => false
                                                            )
                                                        );
                                                        ?>
                                                        <?php } ?> 
                                                    </div>
                                                    <div class="location d-block">
                                                        <span><?= h($su->name); ?> </span>
                                                    </div>
                                                    <div class="location d-block">
                                                        <span><?= h($su->email); ?> </span>
                                                    </div>
                                                    <div class="cartviewprice d-block">
                                                        <span>Location: <?= h($su->location); ?> </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4 col-lg-3 col-xl-2 p-0 qty">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 ml-lg-auto align-self-start mt-2 mt-lg-0">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer border-light cart-panel-foo-fix">
                        </div>
                    </div>
                <?php endforeach; ?>
            <hr>
                <?php foreach ($searchTweet as $st): ?>
                    <div class="card border-light bg-white card proviewcard shadow-sm">
                        <div class="card-header"><h3>Tweet</h3></div>
                            <div class="card gedf-card offset-1 col-md-9 offset-1">   
                                <div class="card-header">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="mr-2">
                                            <?php if (!empty($st->user->image)) : ?>
                                                <?= $this->Html->image("/img/users/" . $st->user->image, ['class' => '', 'width' => '80', 'height' => '80']);?>
                                            <?php else: ?>
                                                <?= $this->Html->image("/img/users/default_picture", ['class' => '', 'width' => '80', 'height' => '80']);?>
                                            <?php endif;?>
                                            </div>
                                            <div class="ml-2">
                                                <?php echo h($st->user->username) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">
                                        <?php echo h($st->content); ?>
                                    </p>
                                </div>
                            </div>
                    </div>
                <?php endforeach; ?>
        </div>
    </div>
</div>
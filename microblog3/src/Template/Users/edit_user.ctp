<div class="container emp-profile">
<?php echo $this->Flash->render()?>
    <h1>Edit Profile</h1>
  	<hr>
	<div class="row">
      <!-- left column -->
      <div class="col-md-2">
      </div>
      
      <!-- edit form column -->
      <div class="col-md-10 personal-info">
        <h3>Personal info</h3>

        <?php
            echo $this->Form->create($users, array(
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
                'novalidate' => true
            ));
        ?>
            <div class="form-group">
                <label class="col-lg-3 control-label">Profile Picture:</label>
                    <div class="col-lg-8">
                        <div class="text-center">
                            <?php
                                echo $this->Form->control('image', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'file',
                                ));
                            ?>
                        </div>
                    </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Full Name:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->control('name', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                    ));
                ?>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Username:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->control('username', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                    ));
                ?>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Password:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->control('password', array(
                        'label' => false,
                        'placeholder' => 'Unchanged',
                        'value' => '',
                        'class' => 'form-control',
                        'type' => 'password',
                    ));
                ?>
                <h6> *Please enter 5 characters and above </h6>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Email Address:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->control('email', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'email',
                    ));
                ?>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Location:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->control('location', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                    ));
                ?>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Birthday:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->control('birthday', [
                        'class' => 'datepicker',
                        // 'data-provide' => "datepicker",
                        'id' => 'datepicker',
                        'label' => false,
                        'value' => date("Y-m-d", strtotime($users->birthday)),
                        'placeholder' => __('YYYY-MM-DD'),
                        'type'=> 'text',
                        ]
                    );
                ?>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Description:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->textarea('description', array(
                    'label' => false,
                    'type' => 'text',
                    'text' => '',
                    'rows' => '4',
                    'maxlength' => '140',
                    'class' => 'form-control',
                    'placeholder' => 'Please enter up to 140 characters',
                    'style' => 'margin-top: 10px'));
                ?>
            </div>
            <div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-8">
                <?= $this->Form->button('Save',[
                    'label' => false,
                    'class'=>'btn btn-primary float-right'
                    ]);?>
                <?= $this->Form->end() ?>
                <span></span>
                <button class="btn btn-secondary"><a href="<?php echo $url ?>users/profile">Cancel </a></button>
            </div>
        </div>
      </div>
  </div>
</div>
<hr>
<script type="text/javascript">
    function readURL(control) {
        if (control.files && control.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(control.files[0]);
        }
    }

        $("#UserImage").change(function() {
        readURL(this);
    });

    $(function () {
        $('#datepicker').datepicker({ dateFormat: 'yy-mm-dd'});
    });
</script>
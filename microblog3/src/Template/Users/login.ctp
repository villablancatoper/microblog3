<div class="container">
<?= $this->Flash->render()?>
	<div class="d-flex justify-content-center h-100">
		<div class="card" style="margin: 100px">
			<div class="card-header" style="background-color: rgb(239, 134, 74)">
				<h3>MICROBLOG</h3>
			</div>
			<div class="card-body" style="width: 420px">
				<div class="user-form">
					<?php echo $this->Form->create() ?>
						<fieldset>
							<?php echo $this->Form->control('username', [
								'label' => false,
								'value' => '',
								'type' => 'text',
								'class' => 'form-control',
								'placeholder' => 'User ID',
								'style' => 'margin-top: 10px']);
								
								echo $this->Form->control('password', [
								'label' => false,
								'value' => '',
								'type' => 'password',
								'class' => 'form-control',
								'placeholder' => 'Password',
								'style' => 'margin-top: 30px']);
							?>
							<div id="inputButton">
                            <?= $this->Form->button('Login',
                                [
                                    'label' => false,
                                    'id' => 'login',
                                    'class' => 'btn btn-primary float-right',
                                    'style' => 'margin-top: 20px'
                                ]);?>
                            <?= $this->Form->end() ?>
						</fieldset>
                       </div>						
				</div>
			<div class="card-footer" style="background-color: rgb(239, 134, 74)">
				<div class="d-flex justify-content-center links">
					Don't have an account?<a href="<?php echo $url ?>register">Sign Up</a>
				</div>
			</div>
		</div>
	</div>
</div>
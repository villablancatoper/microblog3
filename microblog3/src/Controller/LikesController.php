<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Likes Controller
 *
 * @property \App\Model\Table\LikesTable $Likes
 *
 * @method \App\Model\Entity\Like\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LikesController extends AppController
{
    /**
     * Initialization
     * 
     * 
     */
    public function initialize()
    {
        parent::initialize();
    }
    
    /**
     * Like Function
     * 
     * 
     */
    public function like() {
        $this->autoRender = false;
        $this->loadModel('Likes');
        $this->loadModel('Tweets');

        if ($this->request->is('ajax')) {

            $post = $this->request->getData();

            $find = $this->Likes->find('all', [
                'conditions' => [
                    'AND' => [
                        'Likes.tweet_id' => $post['tweet_id'],
                        'Likes.user_id' => $post['user_id']
                    ]
                ]
            ]);
            $find = $find->first();

            $likeTable = TableRegistry::get('Likes');
            $likeData = $likeTable->newEntity();

            if(empty($find)) {
                // if its the first time to like the tweet
                $likeData->tweet_id = $post['tweet_id'];
                $likeData->user_id = $post['user_id'];
                $likeData->is_liked = 1;

            } else if ($find->is_liked == 0) {

                // if liked tweet already exist but not liked
                $likeData->id = $find->id;
                $likeData->tweet_id = $post['tweet_id'];
                $likeData->user_id = $post['user_id'];
                $likeData->is_liked = 1;
            } else if ($find->is_liked == 1) {

                // if liked tweet already exist and is already liked
                $likeData->id = $find->id;
                $likeData->tweet_id = $post['tweet_id'];
                $likeData->user_id = $post['user_id'];
                $likeData->is_liked = 0;
            }
            

            if ($likeTable->save($likeData)) {
                $message = [
                    'data_count' => $this->Likes->find('all', [
                        'conditions' => [
                            'Likes.tweet_id' => $post['tweet_id'],
                            'is_liked' => 1
                        ]
                    ])->count(),
                    'message' => 'Like Successful',
                    'status' => 'success'
                ];
                $this->response->body(json_encode($message));
                return $this->response;

            } else {
                $message = [
                    'message' => 'Like Failed',
                    'status' => 'success'
                ];
                $this->response->body(json_encode($message));
                return $this->response;

            }
        }
    }
}

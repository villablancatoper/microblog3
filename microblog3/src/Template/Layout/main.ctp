<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Microblog';
?>
<!DOCTYPE html>
<html>

<?= $this->Html->script([
      'https://code.jquery.com/jquery-1.12.4.js',
      'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
      '//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js',
      'https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
      'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js',
      '//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js',
      '//code.jquery.com/ui/1.12.1/jquery-ui.js'
    ])
  ?>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<head>
    <?= $this->Html->css([
      'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css',
      'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
      '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
      '//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css'
    ]);
  ?>


    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('profile-style.css'); ?>
    

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <header>
        <?php echo $this->element('header'); ?>
    </header>
    <?= $this->Flash->render() ?>
    <div class="container" style="max-width: 1300px">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>

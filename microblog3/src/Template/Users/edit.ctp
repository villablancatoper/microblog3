<div class="container">
    <div class="container-fluid gedf-wrapper">
        <div class="row">
            <!-- User Details -->
            <div class="col-md-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="h5">@<?php echo $User['User']['username']; ?></div>
                                <div class="h7 text-muted">Fullname : <?php echo h($User['User']['name']); ?></div>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="h6 text-muted">Followers</div>
                                    <div class="h5"><?php echo $isFollowed; ?></div>
                                </li>
                                <li class="list-group-item">
                                    <div class="h6 text-muted">Following</div>
                                    <div class="h5"><?php echo $isFollowing; ?></div>
                                </li>
                                <li class="list-group-item active">
                                    <a href="<?php echo $url ?>users/dashboard">
                                        <i class="fa fa-home"></i>
                                        Home
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="<?php echo $url ?>users/profile">
                                        <i class="fa fa-user"></i>
                                        Profile 
                                    </a>
                                </li>
                                <!-- <li class="list-group-item">
                                    <a href="#">
                                        <i class="fa fa-bell"></i>
                                        Notifications
                                    </a>
                                </li> -->
                                <li class="list-group-item">
                                    <a href="<?php echo $url ?>users/followers">
                                        <i class="fa fa-users"></i>
                                        Followers 
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
            <!-- User Details End -->
            <div class="col-md-7 gedf-main">
                <!-- Main Tweet Section-->
                    <div class="card gedf-card">
                        <div class="card-header">
                            <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Edit Your tweet</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                                    <div class="form-group">
                                        <label class="sr-only" for="message">post</label>
                                        <?php echo $this->Form->create('Tweet');
                                            echo $this->Form->control('retweet_content', array(
                                            'label' => false,
                                            'type' => 'text',
                                            'text' => '',
                                            'rows' => '4',
                                            'maxlength' => '140',
                                            'class' => 'form-control',
                                            'placeholder' => 'Please enter up to 140 characters',
                                            'style' => 'margin-top: 10px'));
                                        ?>
                                    </div>
                                    <div id="controlButton">
                                        <?= $this->Form->control('Save',[
                                            'type' => 'button',
                                            'label' => false,
                                            'class'=>'btn btn-primary float-right'
                                        ]);?>
                                        <?= $this->Form->end() ?>
                                    </div>
                                </div>
                            </div> 
                            <div class="btn-toolbar justify-content-between">
                                <!-- <div class="btn-group">
                                    <button type="submit" class="btn btn-primary float-right">Share</button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="card gedf-card">
                        <div class="card-header">
                            <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Edit Your tweet</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                                    <div class="form-group">
                                        <label class="sr-only" for="message">post</label>
                                        <?php echo $this->Form->create('Tweet');
                                            echo $this->Form->control('content', array(
                                            'label' => false,
                                            'type' => 'text',
                                            'text' => '',
                                            'rows' => '4',
                                            'maxlength' => '140',
                                            'class' => 'form-control',
                                            'placeholder' => 'Please enter up to 140 characters',
                                            'style' => 'margin-top: 10px'));
                                        ?>
                                    </div>
                                    <div id="controlButton">
                                        <?= $this->Form->control('Save',[
                                            'type' => 'button',
                                            'label' => false,
                                            'class'=>'btn btn-primary float-right'
                                        ]);?>
                                        <?= $this->Form->end() ?>
                                    </div>
                                </div>
                            </div> 
                            <div class="btn-toolbar justify-content-between">
                            </div>
                        </div>
                    </div>
                <!-- Main Tweet Section End-->
            </div>                
        </div>
    </div>
</div>

<script type="text/javascript">
</script>
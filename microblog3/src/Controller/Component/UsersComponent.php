<?php

namespace App\Controller\Component;

use Cake\Collection\Collection;
use Cake\Controller\Component;
use App\Model\Entity\Users;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Users component
 */
class UsersComponent extends Component
{
    public function userWhoIsFollowedCount(int $id) {
        $isFollowed = $this->Users->find()
            ->where([
                'Followers.following_id' => $id,
                'Followers.status' => 1
            ])
            ->count();

        return $isFollowed;
    }

    public function userWhoIsFollowingCount(int $id) {
        $isFollowed = $this->Users->find()
            ->where([
                'Followers.user_id' => $id,
                'Followers.status' => 1
            ])
            ->count();

        return $isFollowed;
    }

    public function uploadPhoto (collection $data) {
        $data = $data->first();

        if (!empty($data['picture']) && !empty($data['picture']['tmp_name'])) {
            $uploadData = $data['picture']['tmp_name'];

            $filename = basename($data['picture']['name']);
            $uploadFolder = WWW_ROOT. 'img' ;
            $filename = time() . '_' . $filename;
            $uploadPath =  $uploadFolder . DS . 'users' . DS .  $filename;

            $ext = substr(strtolower(strrchr($data['picture']['name'], '.')), 1); //get the extension
            $arr_ext = ['jpg', 'jpeg', 'gif', 'png']; //set allowed extensions

            $size = $data['picture']['size'];

            // if (!move_uploaded_file($uploadData, $uploadPath) && !in_array($ext, $arr_ext) && $size <= 80000) {
            //     $this->Flash->error(__('Error Uploading Image, Please try again'));
            //     return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
            // }

            if ($size >= 4388608) {
                return 'size';
            }

            if (!in_array($ext, $arr_ext)) {
                return 'extension';
            }

            if (!move_uploaded_file($uploadData, $uploadPath)) {
                return 'path';
            }
            $data['picture'] = $filename;

            return $filename;

        }
        $filename = null;

        if (!empty($data['image']) && !empty($data['image']['tmp_name'])) {
            $uploadData = $data['image']['tmp_name'];

            $filename = basename($data['image']['name']);
            $uploadFolder = WWW_ROOT. 'img' ;
            $filename = time() . '_' . $filename;
            $uploadPath =  $uploadFolder . DS . 'users' . DS .  $filename;

            $ext = substr(strtolower(strrchr($data['image']['name'], '.')), 1); //get the extension
            $arr_ext = ['jpg', 'jpeg', 'gif', 'png']; //set allowed extensions

            $size = $data['image']['size'];

            // if (!move_uploaded_file($uploadData, $uploadPath) && !in_array($ext, $arr_ext) && $size <= 8000000) {
                // $this->Flash->error(__('Error Uploading Image, Please try again'));
                // return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
            // }

            if (!in_array($ext, $arr_ext)) {
                return 'extension';
            }

            if (!move_uploaded_file($uploadData, $uploadPath)) {
                return 'path';
            }

            if ($size >= 8388608) {
                return 'size';
            }
            $data['image'] = $filename;

            return $filename;

        }
        $filename = null;

        return $filename;
    }
}
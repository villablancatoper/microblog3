<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Security;

/**
 * Users Model
 *
 * @property \App\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\FollowersTable&\Cake\ORM\Association\HasMany $Followers
 * @property \App\Model\Table\LikesTable&\Cake\ORM\Association\HasMany $Likes
 * @property \App\Model\Table\TweetsTable&\Cake\ORM\Association\HasMany $Tweets
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Comments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Followers', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Likes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Tweets', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 100)
            ->requirePresence('username', 'create')
            ->notEmptyString('username', 'Please enter your Username')
            ->minLength('username', 5, 'Username must be more than 5 characters')
            ->alphaNumeric('username');

        $validator
            ->scalar('password')
            ->maxLength('password', 100)
            ->requirePresence('password', 'create')
            ->allowEmptyString('password', 'Please enter your password')
            ->add('password', [
                'length' => [
                    'rule' => ['minLength', 5],
                    'message' => 'Password need to be at least 5 characters long',
                ]
            ])
            ->alphaNumeric('password', 'Name must only be alpha numeric characters');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name', 'Please enter your name')
            ->add('name', [
                'alphabethWithSpaces' => [
                    'rule' => ['custom', '/^[a-zA-Z ]*$/i'],
                    'message' => 'Name must only be alphabet with spaces'
                ]
            ]);

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email', 'Please fill our the email form');

        $validator
            ->scalar('location')
            ->maxLength('location', 100)
            ->alphaNumeric('location')
            ->allowEmptyString('location', null);


        $validator
            ->date('birthday');

        $validator
            ->scalar('description')
            ->maxLength('description', 140)
            ->add('description', [
                'alphabethWithSpaces' => [
                    'rule' => ['custom', '/^[a-zA-Z ]*$/i'],
                    'message' => 'Name must only be alpha numeric with spaces'
                ]
            ])
            ->allowEmptyString('description', null);

        //$validator
            //->scalar('image')
            //->maxLength('image', 100)
            // ->requirePresence('image', 'create')
            //->notEmptyFile('image');

        $validator
            ->scalar('activation')
            ->maxLength('activation', 100)
            // ->requirePresence('activation', 'create')
            ->notEmptyString('activation');

        $validator
            ->scalar('status')
            // ->requirePresence('status', 'create')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['username']));
        // $rules->add($rules->isUnique(['username'], 'Username is already taken.'));
        // $rules->add($rules->isUnique(['email']));
        // $rules->add($rules->isUnique(['email'], 'Email Address is already taken.'));

        return $rules;
    }

    /**
     * Before save function
     * 
     * 
     */
    // public function beforeSave() {
    //     if (!empty($this->data['password'])) {
    //         $this->data['password'] = Security::hash($this->data['password']);
    //     }
    //     return true;
    // }
}

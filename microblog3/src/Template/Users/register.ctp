<div class="container">
<!-- <div class="alert alert-warning alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
</div> -->
<?php echo $this->Flash->render(); ?>
<div class="card bg-light">
<article class="card-body mx-auto" style="width: 400px;">
	<h4 class="card-title mt-3 text-center">Create Account</h4>
	<p class="text-center">Get started with your free account</p>
	<?php echo $this->Form->create($user); ?>
		<div class="form-group input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"> <i class="fa fa-at"></i> </span>
			</div>
			<?php echo $this->Form->control('username', array('label' => false, 'size' => '35', 'class' => "form-control-register", 'placeholder' => 'Username', 'style' => 'text-align: -webkit -center font-size: 17px')); ?>
			<span> Please enter 5 characters or more </span>
			<div class="error-message"><?= $error['username'][0] ?? null; ?></div>
			
		</div> <!-- form-group// -->
		<div class="form-group input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"> <i class="fa fa-lock"></i> </span>
			</div>
			<?php echo $this->Form->control('password', array('label' => false, 'size' => '35', 'class' => "form-control-register", 'placeholder' => 'Password', 'type' => 'password')); ?>
			<span> Please enter 5 characters or more </span>
			<div class="error-message"><?= $error['password'][0] ?? null; ?></div>
		</div> <!-- form-group// -->
		<div class="form-group input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
			</div>
			<div>
			<?php echo $this->Form->control('email', array('label' => false, 'size' => '35', 'class' => "form-control-register", 'placeholder' => 'Email Address', 'type' => 'email')); ?>
			</div>
			<div class="error-message"><?= $error['email'][0] ?? null; ?></div>
		</div> <!-- form-group// -->
		<div class="form-group input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"> <i class="fa fa-user"></i> </span>
			</div>
			<?php echo $this->Form->control('name', array('label' => false, 'size' => '35', 'class' => "form-control-register", 'placeholder' => 'Full name')); ?>
			<div class="error-message"><?= $error['name'][0] ?? null; ?></div>
		</div> <!-- form-group// -->                       
		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-block"> Create Account  </button>
		</div> <!-- form-group// -->
		<p class="text-center">Have an account? <a href="<?php echo $url ?>login">Log In</a> </p>
    <?= $this->Form->end() ?>
</article>
</div> <!-- card.// -->
</div> 
<!--container end.//-->